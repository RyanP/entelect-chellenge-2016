declare module "ug" {

  export enum Direction {
    'in' = -1,
    'any' = 0,
    'out' = 1
  }

  interface CompareFunc {
    (node: Node) : boolean
  }

  export interface ClosestOptions {
    compare: CompareFunc
    count?: number
    direction?: Direction
    minDepth?: number
    maxDepth?: number
  }

  export class Unit {
    load(properties: any): Unit
    set(property: string, value: any): any
    unset(property: string): boolean
    has(property: string): boolean
    get(property: string): any
    toString(): string
    valueOf(): string
    entity: string
  } 
  
  export class Node extends Unit {
    unlink(): boolean
    edges: Edge[]
    inputEdges: Edge[]
    outputEdges: Edge[]
  }  
  
  export class Edge extends Unit {
    link(from: Node, to: Node, duplex?: boolean): Edge
    unlink(): boolean
    distance: number
    setDistance(distance: number): Edge
    setWeight(weight: number): Edge
    oppositeNode(node: Node): Node
  }
  
  export class Collection { 
    name(): string
    indices(): string[]
    createIndex(field: string): Collection
    createIndices(fieldList: string[]): Collection
    find(id: string | number)
    find(index: string, id: string | number): Unit
    destroy(id: string | number)
    destroy(index: string, id: string | number): Unit
    query(): Query
  }
  
  export class NodeCollection extends Collection { }

  export class EdgeCollection extends Collection { }

  export class Query extends Collection {
    filter(filtersObjects: any[]): Query
    filter(...filtersObjects: any[]): Query
    exclude(filtersObjects: any[]): Query
    exclude(...filtersObjects: any[]): Query
    first(): Node | Edge
    last(): Node | Edge
    units(): Unit[]
  }
  
  export class Path {
    start(): Node
    end(): Node
    length(): number
    distance(): number
    prettify(): string
    toString(): string
  }

  export class Graph {
    constructor()
    unit(id: number): Unit 
    nodeCount(): number
    edgeCount(): number
    createNode(entity: string, properties: any): Node
    createEdge(entity: string, properties?: any): Edge
    nodes(entity: string): NodeCollection
    edges(entity: string): EdgeCollection
    trace(from: Node, to: Node, direction?: Direction): Path
    closest(node: Node, options: ClosestOptions): Path[]
    toJSON(): string
    fromJSON(): Graph
    save(filename: string, callback: any): Graph
    load(filename: string, callback: any): Graph
  }

}