"use strict";
const core_1 = require('../core');
class Sequence extends core_1.Composite {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        for (var i = 0; i < this.children.length; i += 1) {
            var status = this.children[i].execute(tick);
            if (status !== core_1.State.SUCCESS) {
                return status;
            }
        }
        return core_1.State.SUCCESS;
    }
}
exports.Sequence = Sequence;
//# sourceMappingURL=Sequence.js.map