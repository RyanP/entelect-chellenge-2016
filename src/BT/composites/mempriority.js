"use strict";
const _1 = require('../');
class MemPriority extends _1.Composite {
    constructor(properties, id) {
        super(properties, id);
    }
    open(tick) {
        tick.blackboard.set('runningChild', 0, tick.tree.id, this.id);
    }
    tick(tick) {
        var child = tick.blackboard.get('runningChild', tick.tree.id, this.id);
        for (var i = child; i < this.children.length; i += 1) {
            var status = this.children[i].execute(tick);
            if (status !== _1.State.FAILURE) {
                if (status === _1.State.RUNNING) {
                    tick.blackboard.set('runningChild', i, tick.tree.id, this.id);
                }
                return status;
            }
        }
        return _1.State.FAILURE;
    }
}
exports.MemPriority = MemPriority;
//# sourceMappingURL=mempriority.js.map