"use strict";
const _1 = require('../');
class Priority extends _1.Composite {
    constructor(properties, id) {
        super(properties, id);
        this.children = [];
    }
    tick(tick) {
        for (var i = 0; i < this.children.length; i += 1) {
            var status = this.children[i].execute(tick);
            if (status !== _1.State.FAILURE) {
                return status;
            }
        }
        return _1.State.FAILURE;
    }
}
exports.Priority = Priority;
//# sourceMappingURL=Priority.js.map