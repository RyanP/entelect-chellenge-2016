import { State, BaseNode, Tick, Composite } from '../';

export class Priority extends Composite {
  public children: BaseNode[] = []

  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: Tick) {
    for (var i = 0; i < this.children.length; i += 1) {
      var status: State = this.children[i].execute(tick);

      if (status !== State.FAILURE) {
        return status;
      }
    }

    return State.FAILURE;
  }
}