import { State, BaseNode, Tick, Composite } from '../';

export class MemSequence extends Composite {
  public children: BaseNode[]

  constructor(properties, id?: string) {
    super(properties, id);
  }

  open(tick: Tick) {
    tick.blackboard.set('runningChild', 0, tick.tree.id, this.id);
  }

  tick(tick: Tick) {
    var child = tick.blackboard.get('runningChild', tick.tree.id, this.id);

    for (var i = child; i < this.children.length; i += 1) {
      var status: State = this.children[i].execute(tick);

      if (status !== State.SUCCESS) {
        if (status === State.RUNNING) {
          tick.blackboard.set('runningChild', i, tick.tree.id, this.id);
        }

        return status;
      }
    }

    return State.SUCCESS;
  }
}
