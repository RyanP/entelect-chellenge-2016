import { State, BaseNode, Composite, Tick } from '../core'; 

export class Sequence extends Composite {

  public children: BaseNode[]  

  constructor(properties, id?: string) {
    super(properties, id)
  }

  tick(tick: Tick): State {
    for (var i = 0; i < this.children.length; i += 1) {
      var status: State = this.children[i].execute(tick);

      if (status !== State.SUCCESS) {
        return status;
      }
    }
 
    return State.SUCCESS;
  }
}