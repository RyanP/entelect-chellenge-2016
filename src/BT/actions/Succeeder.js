"use strict";
const _1 = require('../');
class Succeeder extends _1.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        return _1.State.SUCCESS;
    }
}
exports.Succeeder = Succeeder;
//# sourceMappingURL=Succeeder.js.map