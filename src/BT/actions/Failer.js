"use strict";
const _1 = require('../');
class Failer extends _1.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        return _1.State.FAILURE;
    }
}
exports.Failer = Failer;
//# sourceMappingURL=Failer.js.map