import { State, Action, Tick } from '../';

export class Failer extends Action {
  
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: Tick): State {
    return State.FAILURE;
  }
}