"use strict";
const _1 = require('./');
class Decorator extends _1.BaseNode {
    constructor(properties, id) {
        super(properties, id);
        this.category = _1.Category.DECORATOR;
        this.child = this.properties.child;
    }
}
exports.Decorator = Decorator;
//# sourceMappingURL=Decorator.js.map