"use strict";
const _1 = require('./');
const _ = require('lodash');
/**
 * Represents a tick (update) which holds context for the tree traversal.
 */
class Tick {
    constructor(tree, target, blackboard) {
        this._openNodes = [];
        this._nodeCount = 0;
        this.tree = tree;
        this.target = target;
        this.blackboard = blackboard;
    }
    get openNodes() { return this._openNodes; }
    get nodeCount() { return this._nodeCount; }
    /**
     * Push the node to opened nodes.
     */
    enterNode(node) {
        this._nodeCount += 1;
        this._openNodes.push(node);
        if (node.category === _1.Category.COMPOSITE) {
            let pad = _.padStart('', this._openNodes.length, '-');
            console.log(`${pad}> ${node.title}`);
        }
    }
    /**
     * Close the first node.
     */
    closeNode(node) {
        let pad = _.padStart('', this._openNodes.length, '-');
        this._openNodes.pop();
        console.log(`${pad}> ${node.title} -> ${node.status} `);
    }
    // Extended for debugging
    openNode(node) {
    }
    tickNode(node) { }
    exitNode(node) {
        //console.log(`BT: exitNode(${node.title}): ${node.status}`)
    }
}
exports.Tick = Tick;
//# sourceMappingURL=Tick.js.map