export enum Category { 
  ACTION,
  COMPOSITE,
  CONDITION,
  DECORATOR
}

export enum State {
  SUCCESS,
  RUNNING,
  FAILURE,
  ERROR
}