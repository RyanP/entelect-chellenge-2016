/**
 * module behavior3
 */
"use strict";
const core = require('../');
const _1 = require('../');
/**
 * A behavior tree.
 */
class BehaviorTree {
    constructor() {
        this.id = _1.createUUID();
    }
    /**
     * Apply the behavior tree to an entity with a context blackboard.
     */
    tick(target, blackboard) {
        var tick = new core.Tick(this, target, blackboard);
        // execute the whole tree
        this.root.execute(tick);
        var lastOpenNodes = blackboard.get('openNodes', this.id) || [];
        var currOpenNodes = tick.openNodes.slice(0) || [];
        var start = 0;
        var max = Math.min(lastOpenNodes.length, currOpenNodes.length);
        // does not close if still open in this tick
        for (var i = 0; i < max; i += 1) {
            start = i + 1;
            if (lastOpenNodes[i] !== currOpenNodes[i]) {
                break;
            }
        }
        // close the nodes that are still opened
        for (var i = lastOpenNodes.length - 1; i >= start; i -= 1) {
            lastOpenNodes[i]._close(tick);
        }
        blackboard.set('openNodes', currOpenNodes, this.id);
        blackboard.set('nodeCount', tick.nodeCount, this.id);
    }
    load(data, names) {
        names = names || {};
        // this.title = data.title || this.title;
        // this.description = data.description || this.description;
        // this.properties = data.properties || this.properties;
        var nodes = {};
        var id, spec, node;
        // Create the node list (without connection between them)
        for (id in data.nodes) {
            spec = data.nodes[id];
            var Cls;
            if (spec.name in names) {
                // Look for the name in custom nodes
                Cls = names[spec.name];
            }
            else if (spec.name in core) {
                // Look for the name in default nodes
                Cls = core[spec.name];
            }
            else {
                // Invalid node name
                throw new EvalError('BehaviorTree.load: Invalid node name + "' +
                    spec.name + '".');
            }
            spec.properties.title = spec.title;
            node = new Cls(spec.properties, spec.id);
            nodes[id] = node;
        }
        // Connect the nodes
        for (id in data.nodes) {
            spec = data.nodes[id];
            node = nodes[id];
            if (node.category === core.Category.COMPOSITE && spec.children) {
                for (var i = 0; i < spec.children.length; i++) {
                    var cid = spec.children[i];
                    node.children.push(nodes[cid]);
                }
            }
            else if (node.category === core.Category.DECORATOR && spec.child) {
                node.child = nodes[spec.child];
            }
        }
        this.root = nodes[data.root];
    }
}
exports.BehaviorTree = BehaviorTree;
//# sourceMappingURL=BehaviorTree.js.map