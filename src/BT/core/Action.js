"use strict";
const _1 = require('./');
//import { BaseNode } from './BaseNode'
class Action extends _1.BaseNode {
    constructor(properties, id) {
        super(properties, id);
        this.category = _1.Category.ACTION;
    }
}
exports.Action = Action;
//# sourceMappingURL=Action.js.map