
import { createUUID } from '../';
import { State, Category, Tick } from './';

export class BaseNode {
  public category: Category
  private _id: string
  private _title: string
  private _description: string
  private _status: State
  protected _properties: any = {}
  
  get id(): string { return this._id }  
  get name(): string { return this.constructor.name }
  get title(): string { return this._title || this.name }
  get description(): string { return this._description }
  get properties(): any { return this._properties }
  get status(): string { return State[this._status] }

  constructor(properties, id?: string) {
    this._properties = properties || null
    this._title = properties.title || null
    this._description = properties.description || null
    this._id = id || createUUID();
  }

  public execute(tick: Tick) : State { 
    this._enter(tick);

    if (!tick.blackboard.get('isOpen', tick.tree.id, this.id)) {
      this._open(tick);
    }

    this._status = this._tick(tick);

    if (this._status !== State.RUNNING) {
      this._close(tick);
    }

    this._exit(tick);

    return this._status;
  }

  private _enter(tick: Tick) {
    tick.enterNode(this);
    this.enter(tick);
  }

  private _open(tick: Tick) {
    tick.openNode(this);
    tick.blackboard.set('isOpen', true, tick.tree.id, this.id);
    this.open(tick);
  }

  private _tick(tick: Tick): State {
    tick.tickNode(this);
    return this.tick(tick);
  }

  private _close(tick: Tick) {
    tick.closeNode(this);
    tick.blackboard.set('isOpen', false, tick.tree.id, this.id);
    this.close(tick);
  }

  private _exit(tick: Tick) {
    tick.exitNode(this);
    this.exit(tick);
  }

  // to be overriden
  public enter(tick: Tick) {}
  public open(tick: Tick) {}
  public tick(tick: Tick): State { return }
  public close(tick: Tick) {}
  public exit(tick: Tick) {}
}
