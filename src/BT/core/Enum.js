"use strict";
(function (Category) {
    Category[Category["ACTION"] = 0] = "ACTION";
    Category[Category["COMPOSITE"] = 1] = "COMPOSITE";
    Category[Category["CONDITION"] = 2] = "CONDITION";
    Category[Category["DECORATOR"] = 3] = "DECORATOR";
})(exports.Category || (exports.Category = {}));
var Category = exports.Category;
(function (State) {
    State[State["SUCCESS"] = 0] = "SUCCESS";
    State[State["RUNNING"] = 1] = "RUNNING";
    State[State["FAILURE"] = 2] = "FAILURE";
    State[State["ERROR"] = 3] = "ERROR";
})(exports.State || (exports.State = {}));
var State = exports.State;
//# sourceMappingURL=Enum.js.map