"use strict";
const _1 = require('./');
class Condition extends _1.BaseNode {
    constructor(properties, id) {
        super(properties, id);
        this.category = _1.Category.CONDITION;
    }
}
exports.Condition = Condition;
//# sourceMappingURL=Condition.js.map