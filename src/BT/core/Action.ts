import { BaseNode, Category } from './'
//import { BaseNode } from './BaseNode'

export class Action extends BaseNode {

  public category: Category 

  constructor(properties, id?: string) {
    super(properties, id)
    this.category = Category.ACTION
  }

}