import { Category, BehaviorTree, BaseNode, BlackBoard } from './'
import Types from './'
import * as _ from 'lodash'
/**
 * Represents a tick (update) which holds context for the tree traversal.
 */
export class Tick {

  public tree: BehaviorTree
  public target: any
  public blackboard: BlackBoard
  protected _openNodes: BaseNode[] = []
  protected _nodeCount: number = 0

  get openNodes(): BaseNode[] { return this._openNodes }
  get nodeCount(): Number { return this._nodeCount }

  constructor(tree: BehaviorTree, target: any, blackboard: BlackBoard) {
    this.tree = tree;
    this.target = target;
    this.blackboard = blackboard;
  }

  /**
   * Push the node to opened nodes.
   */
   public enterNode(node: BaseNode) {
    this._nodeCount += 1;
    this._openNodes.push(node);
    if (node.category === Category.COMPOSITE) {
      let pad = _.padStart('', this._openNodes.length, '-');
      console.log(`${pad}> ${node.title}`)
    }
  }

  /**
   * Close the first node.
   */
   public closeNode(node: BaseNode) {
    let pad = _.padStart('', this._openNodes.length, '-');
    this._openNodes.pop();
    console.log(`${pad}> ${node.title} -> ${node.status} `)
   }
  
  // Extended for debugging
  public openNode(node: BaseNode) {
    
  }
  public tickNode(node: BaseNode) { }
  
  public exitNode(node: BaseNode) {
    //console.log(`BT: exitNode(${node.title}): ${node.status}`)
  }
}
