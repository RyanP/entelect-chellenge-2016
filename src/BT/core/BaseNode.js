"use strict";
const _1 = require('../');
const _2 = require('./');
class BaseNode {
    constructor(properties, id) {
        this._properties = {};
        this._properties = properties || null;
        this._title = properties.title || null;
        this._description = properties.description || null;
        this._id = id || _1.createUUID();
    }
    get id() { return this._id; }
    get name() { return this.constructor.name; }
    get title() { return this._title || this.name; }
    get description() { return this._description; }
    get properties() { return this._properties; }
    get status() { return _2.State[this._status]; }
    execute(tick) {
        this._enter(tick);
        if (!tick.blackboard.get('isOpen', tick.tree.id, this.id)) {
            this._open(tick);
        }
        this._status = this._tick(tick);
        if (this._status !== _2.State.RUNNING) {
            this._close(tick);
        }
        this._exit(tick);
        return this._status;
    }
    _enter(tick) {
        tick.enterNode(this);
        this.enter(tick);
    }
    _open(tick) {
        tick.openNode(this);
        tick.blackboard.set('isOpen', true, tick.tree.id, this.id);
        this.open(tick);
    }
    _tick(tick) {
        tick.tickNode(this);
        return this.tick(tick);
    }
    _close(tick) {
        tick.closeNode(this);
        tick.blackboard.set('isOpen', false, tick.tree.id, this.id);
        this.close(tick);
    }
    _exit(tick) {
        tick.exitNode(this);
        this.exit(tick);
    }
    // to be overriden
    enter(tick) { }
    open(tick) { }
    tick(tick) { return; }
    close(tick) { }
    exit(tick) { }
}
exports.BaseNode = BaseNode;
//# sourceMappingURL=BaseNode.js.map