import { BaseNode, Category } from './'

export class Decorator extends BaseNode {

  public category: Category 
  public child: BaseNode

  constructor(properties, id?: string) {
    super(properties, id)
    this.category = Category.DECORATOR
    this.child = this.properties.child
  }

}