"use strict";
const _1 = require('./');
class Composite extends _1.BaseNode {
    constructor(properties, id) {
        super(properties, id);
        this.children = (this.properties.children || []).slice(0);
        this.category = _1.Category.COMPOSITE;
    }
}
exports.Composite = Composite;
//# sourceMappingURL=Composite.js.map