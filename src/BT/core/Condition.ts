import { BaseNode, Category } from './'

export class Condition extends BaseNode {

  public category: Category 

  constructor(properties, id?: string) {
    super(properties, id)
    this.category = Category.CONDITION
  }

}