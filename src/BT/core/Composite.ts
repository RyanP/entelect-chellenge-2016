import { BaseNode, Category } from './'

export class Composite extends BaseNode {

  public category: Category
  public children: BaseNode[]

  constructor(properties, id?: string) {
    super(properties, id)
    this.children = (this.properties.children || []).slice(0);
    this.category = Category.COMPOSITE
  }

}