"use strict";
const _1 = require('../');
class Inverter extends _1.Decorator {
    constructor(properties, id) {
        super(properties, id);
        if (!this.child) {
            console.warn(`[behavior3] a decorator was initialized without child (${super.id})`);
        }
    }
    tick(tick) {
        if (!this.child) {
            return _1.State.FAILURE;
        }
        var status = this.child.execute(tick);
        if (status === _1.State.SUCCESS) {
            return _1.State.FAILURE;
        }
        else if (status === _1.State.FAILURE) {
            return _1.State.SUCCESS;
        }
        return status;
    }
}
exports.Inverter = Inverter;
//# sourceMappingURL=inverter.js.map