import { State, BaseNode, Tick, Decorator } from '../';

export class Inverter extends Decorator {
  public child: BaseNode

  constructor(properties, id?: string) {
    super(properties, id);

    if (!this.child) {
      console.warn(`[behavior3] a decorator was initialized without child (${super.id})`);
    }
  }

  tick(tick: Tick) : State {
    if (!this.child) {
      return State.FAILURE;
    }

    var status: State = this.child.execute(tick);

    if (status === State.SUCCESS) {
      return State.FAILURE;
    } else if (status === State.FAILURE) {
      return State.SUCCESS;
    }

    return status;
  }
}