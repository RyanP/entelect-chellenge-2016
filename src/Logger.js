"use strict";
const moment = require('moment');
function log() {
    var now = moment();
    var out = [now.format('YYYY-MM-DD HH:mm:ss:SSS'), '\t[BOT]', '\t'];
    for (var i = 0; i < arguments.length; i++) {
        out.push(arguments[i]);
    }
    console.log.apply(this, out);
}
exports.log = log;
function logErr() {
    var now = moment();
    var out = [now.format('YYYY-MM-DD HH:mm:ss:SSS'), '\t[BOT]', '\t'];
    for (var i = 0; i < arguments.length; i++) {
        out.push(arguments[i]);
    }
    console.error.apply(this, out);
}
exports.logErr = logErr;
//# sourceMappingURL=Logger.js.map