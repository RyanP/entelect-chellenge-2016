'use strict';

import * as fs from 'fs'
import * as moment from 'moment'
import * as path from 'path'
import * as _ from 'lodash'

import { debugOpts, cfg } from './Config'
import * as Logger from './Logger' 

import { AI } from "./AI"
import { GameGraph } from "./GameGraph"

module.exports = main;

function main(botKey, outputPath) {
    var startTime = moment();
    Logger.log('Started.');
    Logger.log('Node.js version: ' + process.version);

    var state = loadState(outputPath);
    logPlayerState(state, botKey);

    let graph = new GameGraph(botKey, state)
    let bot = new AI(graph)
    let move = bot.getMove()

    outputMove(move, outputPath, function () {
        var endTime = moment();
        var runTime = endTime.diff(startTime);
        Logger.log('Finished in ' + runTime + 'ms.');
    });
}

function loadState(outputPath) {
    var filename = path.join(outputPath, cfg.stateFile);

    if (!fs.existsSync(filename)) {
        Logger.logErr('State file not found: ' + filename);
        return null;
    }

    if (debugOpts.useDefaultState) {
        var fileContents = fs.readFileSync(filename, { encoding: 'utf8' });
    } else {
        var stateFilePath = path.join(
            debugOpts.replayPath,
            debugOpts.seed,
            debugOpts.round,
            cfg.stateFile
        )
        var fileContents = fs.readFileSync(stateFilePath, { encoding: 'utf8' });
    }

    var gameState = JSON.parse(fileContents.trim());

    return gameState;
}

function logPlayerState(state, botKey) {
    if (state === null) {
        Logger.logErr('Failed to load state.');
        return;
    }

    var myPlayer = null;
    for (var i = 0; i < state.RegisteredPlayerEntities.length; i++) {
        var player = state.RegisteredPlayerEntities[i];

        if (player.Key === botKey) {
            myPlayer = player;
        }
    }

    if (myPlayer !== null) {
        Logger.log('Player state:')
        Logger.log('--Name: ', myPlayer.Name);
        Logger.log('--Points: ', myPlayer.Points);
        Logger.log('--Alive: ', !myPlayer.Killed);
        Logger.log('--Location: (', myPlayer.Location.X + ', ' + myPlayer.Location.Y + ')');
    }
}

function outputMove(move, outputPath, callback) {
    var filename = path.join(outputPath, cfg.moveFile);

    if (!fs.existsSync(outputPath)) {
        fs.mkdirSync(outputPath);
    }

    fs.writeFile(filename, move, function (err) {
        if (err) {
            Logger.logErr('Failed to write ' + filename + '!'); 
            Logger.logErr(err.stack);
            return;
        }

        Logger.log('Move: ' + move);

        if ((callback !== null) && (_.isFunction(callback))) {
            callback();
        }
    });
}