import * as moment from 'moment'

export function log() { 
    var now = moment();

    var out = [now.format('YYYY-MM-DD HH:mm:ss:SSS'), '\t[BOT]', '\t'];
    for (var i = 0; i < arguments.length; i++) {
        out.push(arguments[i]);
    }

    console.log.apply(this, out);
}

export function logErr() {
    var now = moment(); 

    var out = [now.format('YYYY-MM-DD HH:mm:ss:SSS'), '\t[BOT]', '\t'];
    for (var i = 0; i < arguments.length; i++) {
        out.push(arguments[i]);
    }

    console.error.apply(this, out);
}
