'use strict';
const fs = require('fs');
const moment = require('moment');
const path = require('path');
const _ = require('lodash');
const Config_1 = require('./Config');
const Logger = require('./Logger');
const AI_1 = require("./AI");
const GameGraph_1 = require("./GameGraph");
module.exports = main;
function main(botKey, outputPath) {
    var startTime = moment();
    Logger.log('Started.');
    Logger.log('Node.js version: ' + process.version);
    var state = loadState(outputPath);
    logPlayerState(state, botKey);
    let graph = new GameGraph_1.GameGraph(botKey, state);
    let bot = new AI_1.AI(graph);
    let move = bot.getMove();
    outputMove(move, outputPath, function () {
        var endTime = moment();
        var runTime = endTime.diff(startTime);
        Logger.log('Finished in ' + runTime + 'ms.');
    });
}
function loadState(outputPath) {
    var filename = path.join(outputPath, Config_1.cfg.stateFile);
    if (!fs.existsSync(filename)) {
        Logger.logErr('State file not found: ' + filename);
        return null;
    }
    if (Config_1.debugOpts.useDefaultState) {
        var fileContents = fs.readFileSync(filename, { encoding: 'utf8' });
    }
    else {
        var stateFilePath = path.join(Config_1.debugOpts.replayPath, Config_1.debugOpts.seed, Config_1.debugOpts.round, Config_1.cfg.stateFile);
        var fileContents = fs.readFileSync(stateFilePath, { encoding: 'utf8' });
    }
    var gameState = JSON.parse(fileContents.trim());
    return gameState;
}
function logPlayerState(state, botKey) {
    if (state === null) {
        Logger.logErr('Failed to load state.');
        return;
    }
    var myPlayer = null;
    for (var i = 0; i < state.RegisteredPlayerEntities.length; i++) {
        var player = state.RegisteredPlayerEntities[i];
        if (player.Key === botKey) {
            myPlayer = player;
        }
    }
    if (myPlayer !== null) {
        Logger.log('Player state:');
        Logger.log('--Name: ', myPlayer.Name);
        Logger.log('--Points: ', myPlayer.Points);
        Logger.log('--Alive: ', !myPlayer.Killed);
        Logger.log('--Location: (', myPlayer.Location.X + ', ' + myPlayer.Location.Y + ')');
    }
}
function outputMove(move, outputPath, callback) {
    var filename = path.join(outputPath, Config_1.cfg.moveFile);
    if (!fs.existsSync(outputPath)) {
        fs.mkdirSync(outputPath);
    }
    fs.writeFile(filename, move, function (err) {
        if (err) {
            Logger.logErr('Failed to write ' + filename + '!');
            Logger.logErr(err.stack);
            return;
        }
        Logger.log('Move: ' + move);
        if ((callback !== null) && (_.isFunction(callback))) {
            callback();
        }
    });
}
//# sourceMappingURL=main.js.map