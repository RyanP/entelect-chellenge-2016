import { Graph as UnitGraph, Node, Edge, Unit, Collection, NodeCollection, EdgeCollection, Query } from "ug"
import * as _ from "lodash"
const Path = require('../../node_modules/ug/lib/path.js');

export { Node, Edge, Unit, Collection, NodeCollection, EdgeCollection, Query, Path } from "ug"

// Path.prototype.distance = function () {
//   return this._raw.filter(function (v, i) {
//     return !!(i & 1);
//   }).reduce(function (p, c) {
//     return p + c.distance;
//   }, 0);
// };

export class Graph extends UnitGraph {
  private _nodes
  private _nodeCollections
  private _edgeCollections

  constructor() {
    super()
  }

  public node(id: string): Node {
    return _(this._nodes)
      .find(node => {
        return node.get('id') === id
      })
  }

  public get edgeCollections() {
    return this._edgeCollections
  }

  public get nodeCollections() {
    return this._nodeCollections
  }
 
  closest(node, opts) {

    opts = opts || {};

    return this._search(node, opts.compare, opts.count, opts.direction, opts.minDepth, opts.maxDepth);

  }

  trace(fromNode, toNode, direction) {

    let passCondition = function (node) {
      return node === toNode; 
    };

    return this._search(fromNode, passCondition, 1, direction)[0] || new Path([]);

  }

  _search(node, passCondition, count, direction, minDepth?, maxDepth?) {

    passCondition = (typeof passCondition === 'function') ? passCondition : function (node) {
      return true;
    };

    direction |= 0;
    count = Math.max(0, count | 0);
    minDepth = Math.max(0, minDepth | 0);
    maxDepth = Math.max(0, maxDepth | 0);

    let nodePath = Object.create(null);
    nodePath[node.__uniqid__] = [node];


    let depthMap = new Map();
    depthMap.set(0, [node]);

    let depthList = [0];

    let found = [];
    let getPath = this._getPath;

    function enqueue(node, depth) {
      depthMap.has(depth) ?
        depthMap.get(depth).push(node) :
        depthMap.set(depth, [node]);
      orderedSetInsert(depthList, depth);
    }

    function orderedSetInsert(arr, val) {
      // fix bug in original library by using lodash for orderend insert
      return arr.splice( _.sortedIndex( arr, val ), 0, val );
    }

    function readNode(node, curDepth) {

      let edges = (direction === 0 ? node.edges : direction > 0 ? node.outputEdges : node.inputEdges);

      for (let i = 0, len = edges.length; i < len; i++) {

        let edge = edges[i];
        let depth = curDepth + edge.distance;

        // filter out nodes with a distance greater that max depth
        // or with an Infinate depth only if they're not the 1st or
        // last nodes 
        //if ((maxDepth && depth > maxDepth)) {      
        if ((maxDepth && depth > maxDepth)) {
          continue;
        }

        let tnode = edges[i].oppositeNode(node);

        if (!nodePath[tnode.__uniqid__]) {

          nodePath[tnode.__uniqid__] = [edge, tnode];
          enqueue(tnode, depth);

        }

      }

      if (curDepth >= minDepth && passCondition(node)) {
        return new Path(getPath(node, nodePath));
      }

      return false;

    }

    while (depthList.length) {

      let curDepth = depthList.shift();
      let queue = depthMap.get(curDepth);

      while (queue.length) {

        let path = readNode(queue.shift(), curDepth);
        path && found.push(path);

        if (count && found.length >= count) {
          return found;
        }

      }

    }

    return found;

  }

}