"use strict";
const colors = require('colors');
//import * as GraphLib from "graphlib"
const Graph_1 = require("./Graph");
const _ = require("lodash");
const Types = require("../Entity/Types");
const EntityFactory_1 = require("../Entity/EntityFactory");
var Graph_2 = require("./Graph");
exports.Node = Graph_2.Node;
exports.Edge = Graph_2.Edge;
exports.Unit = Graph_2.Unit;
exports.Collection = Graph_2.Collection;
exports.NodeCollection = Graph_2.NodeCollection;
exports.EdgeCollection = Graph_2.EdgeCollection;
exports.Query = Graph_2.Query;
exports.Path = Graph_2.Path;
const dirs = [[1, 0], [0, 1], [-1, 0], [0, -1]];
(function (Direction) {
    Direction[Direction["IN"] = -1] = "IN";
    Direction[Direction["ANY"] = 0] = "ANY";
    Direction[Direction["OUT"] = 1] = "OUT";
})(exports.Direction || (exports.Direction = {}));
var Direction = exports.Direction;
class GameGraph extends Graph_1.Graph {
    constructor(playerKey, gameState) {
        super();
        this.playerKey = playerKey;
        this._bombBlastNodes = {};
        const g = this;
        this.playerKey = playerKey;
        this.numberOfPlayers = gameState.RegisteredPlayerEntities.length;
        this.nodes('Player').createIndex('id').createIndex('key');
        this.nodes('Bomb').createIndex('id');
        this.nodes('Empty').createIndex('id');
        this.nodes('PowerUp').createIndex('id');
        this.nodes('DestructibleWall').createIndex('id');
        // add player nodes first so we can map any existing bombs to them later.
        _.flattenDeep(gameState.GameBlocks)
            .filter((o) => o.Entity && o.Entity.$type == "Domain.Entities.PlayerEntity, Domain")
            .forEach((o) => {
            let entity = EntityFactory_1.EntityFactory.create(o);
            if (entity.Bomb) {
                g.createNode('Bomb', { id: entity.Bomb.id, entity: entity.Bomb });
            }
            g.createNode(entity.constructor.name, { id: entity.id, key: entity.Key, entity: entity });
        });
        // generate graph
        _(gameState.GameBlocks)
            .flattenDeep()
            .filter((block) => {
            if (block.Entity == null)
                return true;
            return block.Entity.$type && block.Entity.$type !== "Domain.Entities.IndestructibleWallEntity, Domain";
        })
            .map((block) => {
            let node = g.node(`${block.Location.X},${block.Location.Y}`);
            if (node)
                return node;
            let entity = EntityFactory_1.EntityFactory.create(block);
            let ctor;
            if (entity.constructor.name == 'SuperPowerUp' || entity.constructor.name == 'BombBagPowerUp' || entity.constructor.name == 'BombRadiusPowerUp') {
                ctor = 'PowerUp';
            }
            return g.createNode(ctor || entity.constructor.name, { id: entity.id, entity: entity });
        })
            .map((currentNode) => {
            if (!currentNode)
                return;
            let currentEntity = currentNode.get('entity');
            let neighbourLocations = currentEntity.Location.neighbours;
            neighbourLocations.forEach(location => {
                let neighbourNode = g.node(location.toString());
                if (neighbourNode) {
                    let neighbourEntity = neighbourNode.get('entity');
                    g.createEdge('Path', { from: currentNode.get('id'), to: neighbourNode.get('id') })
                        .link(currentNode, neighbourNode)
                        .setDistance(1);
                }
            });
        })
            .value();
        let walls = this.nodes('DestructibleWall').query().units();
        _(walls).each((wall) => this.setDestructibleWallWeights(wall));
        let players = this.nodes('Player').query().units();
        _(players).each((player) => this.setPlayerWeights(player));
        let bombs = this.nodes('Bomb').query().units();
        this.addAllBlastNodes(bombs);
        _(bombs).each((bomb) => this.setBombBlastWeights(bomb));
    }
    setDestructibleWallWeights(wallNode) {
        _(wallNode.inputEdges)
            .each(edge => {
            return edge.setDistance(25);
        });
    }
    setPlayerWeights(playerNode) {
        _(playerNode.inputEdges)
            .each(edge => edge.setDistance(100));
    }
    addAllBlastNodes(bombNodes) {
        _(bombNodes).each((bombNode) => {
            let bomb = bombNode.get('entity');
            let blastNodes = this.getNodesInRadius(bomb.Location, bomb.BombRadius);
            this._bombBlastNodes[bomb.id] = blastNodes;
        });
    }
    setBombBlastWeights(bombNode) {
        let g = this;
        let bomb = bombNode.get('entity');
        let bombOwner = this.getPlayer(bomb.Owner);
        // set bomb weight to Infinity
        _(bombNode.inputEdges).each(edge => edge.setDistance(200));
        // set weight of all nodes in this bomb's blast radius.
        let blastNodes = this._bombBlastNodes[bomb.id];
        // set weight proportionally to danger of explosion
        let weight = bomb.BombTimer > 2 ? Math.abs(12 - bomb.BombTimer) : 200;
        // reset weight if bomb could be detonated by enemy
        if (bombOwner && bombOwner.Key !== g.hero.Key) {
            // is this bomb in a chain
            let bombsInChain = _(blastNodes)
                .filter(blastLocation => g.node(blastLocation).entity === 'Bomb')
                .value();
            let minChainedBomb = g.getPlayerBombWithLowestTimer(bombOwner.Key);
            if (bombsInChain.length && bomb.Owner === minChainedBomb.Owner) {
                // is a bomb that could be detonated
                if (!g.isPlayerInBombRadius(g.hero, bomb)) {
                    // only if hero is not in the blast radius of this bomb, otherwise
                    // this gets in the way of calculating an escape path.
                    weight = 150;
                }
                else {
                    weight = 1;
                }
            }
        }
        _(blastNodes)
            .each(blastNodeId => {
            let blastNode = g.node(blastNodeId);
            let inOwnBlast = this.isLocationInOwnBlastRadius(blastNodeId);
            let heroInOwnBlast = this.isInOwnBlastRadius(g.hero);
            // skip bomb nodes as we don't want to override their input weights
            if (blastNode.entity !== 'Empty') {
                return;
            }
            _(blastNode.inputEdges).each(edge => {
                // reset weight if we're currently on our own bomb radius so we can escape
                let finalWeight = (inOwnBlast || heroInOwnBlast) && !(weight >= 100) ? 1 : weight;
                let oppositeNode = edge.oppositeNode(blastNode);
                if (oppositeNode.entity == 'DestructibleWall') {
                    if (!_.includes(blastNodes, oppositeNode.get('id'))) {
                        let wallInEdge = g.edges('Path')
                            .query().filter({
                            from__is: blastNode.get('id'),
                            to__is: oppositeNode.get('id')
                        })
                            .first();
                        wallInEdge.setDistance(wallInEdge.distance + 100);
                    }
                }
                edge.setDistance(finalWeight);
            });
        });
        blastNodes.push(bomb.id);
    }
    getPlayer(key) {
        let result = this.nodes('Player').find('key', key);
        return result ? result.get('entity') : undefined;
    }
    get players() {
        return _(this.nodes('Player').query().units())
            .map(node => node.get('entity'))
            .value();
    }
    get otherPlayers() {
        let key = this.playerKey;
        let playerNodes = this.nodes('Player').query().exclude({ key__is: key }).units();
        return _(playerNodes)
            .map((node) => node.get('entity'))
            .value();
    }
    get hero() {
        return this.getPlayer(this.playerKey);
    }
    get bombs() {
        return _(this.nodes('Bomb').query().units())
            .map(node => node.get('entity'))
            .value();
    }
    get heroBombs() {
        let hero = this.hero;
        let bombs = _(this.nodes('Bomb').query().filter({ entity__Owner__is: hero.Key }).units())
            .map(bomb => bomb.get('entity'))
            .value();
        return bombs ? bombs : undefined;
    }
    getPlayerBombWithLowestTimer(playerKey) {
        if (!playerKey)
            return undefined;
        let bomb = _(this.bombs)
            .filter(b => b.Owner === playerKey && b.BombTimer > 1)
            .minBy((b) => b.BombTimer);
        return bomb ? bomb : undefined;
    }
    get bombBlastNodes() {
        return this._bombBlastNodes;
    }
    get powerUps() {
        // return _.filter(this._entities, (o: Node) =>
        //   o.Entity instanceof Types.SuperPowerUp ||
        //   o.Entity instanceof Types.BombRadiusPowerUp ||
        //   o.Entity instanceof Types.BombBagPowerUp
        // )
        //   .map((o: Node) => o.Entity)
        return _(this.nodes('PowerUp').query().units())
            .map(node => node.get('entity'))
            .value();
    }
    getAt(xOrgoal, y) {
        if (typeof xOrgoal == 'number') {
            return this.node(`${xOrgoal},${y}`);
        }
        else if (xOrgoal instanceof Types.Location || typeof xOrgoal == "string") {
            return this.node(xOrgoal.toString());
        }
    }
    getEntityAt(xOrgoal, y) {
        if (typeof xOrgoal == 'number') {
            return this.node(`${xOrgoal},${y}`) ? this.node(`${xOrgoal},${y}`).get('entity') : undefined;
        }
        else if (xOrgoal instanceof Types.Location || typeof xOrgoal == "string") {
            return this.node(xOrgoal.toString()) ? this.node(xOrgoal.toString()).get('entity') : undefined;
        }
    }
    isInBlastRadius(idOrLocationOrNodeOrPlayer) {
        let _id;
        if (typeof idOrLocationOrNodeOrPlayer === 'string')
            _id = idOrLocationOrNodeOrPlayer;
        if (idOrLocationOrNodeOrPlayer instanceof Types.Location)
            _id = idOrLocationOrNodeOrPlayer.toString();
        if (idOrLocationOrNodeOrPlayer.constructor == Graph_1.Node)
            _id = idOrLocationOrNodeOrPlayer.get('entity').id;
        if (idOrLocationOrNodeOrPlayer.constructor == Types.Player)
            _id = idOrLocationOrNodeOrPlayer.id;
        let result = _(this.bombBlastNodes).values().flatten().includes(_id);
        return result ? true : false;
    }
    isInOwnBlastRadius(player) {
        let bombs = this.getBombsFromBlastLocation(player.id);
        if (bombs) {
            bombs = _.filter(bombs, bomb => bomb.Owner === player.Key);
        }
        return bombs && bombs.length > 0;
    }
    isLocationInOwnBlastRadius(location) {
        let g = this;
        let bombs = this.getBombsFromBlastLocation(location);
        if (bombs) {
            bombs = _.filter(bombs, bomb => bomb.Owner === g.hero.Key);
        }
        return bombs && bombs.length > 0;
    }
    isPlayerInBombRadius(player, bomb) {
        let playerBombIds = _(this.bombBlastNodes[bomb.id]);
        return _(playerBombIds).includes(player.id);
    }
    getBombsFromBlastLocation(location) {
        let g = this;
        let bomb = this.node(location.toString());
        if (bomb && bomb.entity === 'Bomb')
            return [bomb.get('entity')];
        let result = _(this.nodes('Bomb').query().units())
            .map(node => node.get('entity'))
            .filter(b => _.includes(this.bombBlastNodes[b.id], location.toString()))
            .value();
        return result && result.length ? result : undefined;
    }
    getChainedBombsMinTimer(location) {
        let g = this;
        let findChainedBombs = function (bomb) {
            // find bombs that have this bomb in their radius
            let bombs = _(g.nodes('Bomb').query().units())
                .map(node => node.get('entity'))
                .filter(b => _.includes(g.bombBlastNodes[b.id], bomb.id))
                .value();
            return bombs;
        };
        let bombs = [];
        let found = [];
        let queue = [];
        let minTimer = Infinity;
        let minBomb;
        let first = this.getBombsFromBlastLocation(location.toString());
        if (first)
            queue = first;
        while (queue.length) {
            let bomb = queue.pop();
            if (_.includes(found, bomb.id))
                continue;
            let chained = findChainedBombs(bomb);
            if (!chained || chained.length === 1)
                minTimer = bomb.BombTimer;
            found.push(bomb.id);
            let isOwnBomb = bomb.Owner === g.hero.Key;
            let enemy = g.getPlayer(bomb.Owner);
            let isEnemyOnOwnBomb = enemy.Location.toString() === bomb.Location.toString();
            if (!isOwnBomb) {
                minBomb = bomb;
                if (!isEnemyOnOwnBomb) {
                    if (g.isInOwnBlastRadius(enemy)) {
                        // get enemy dist to closest safe node
                        let enemySafeNodes = g.closest(g.node(enemy.id), {
                            compare: (node) => {
                                if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                                    if (!g.isInBlastRadius(node.get('id'))) {
                                        return true;
                                    }
                                }
                                return false;
                            },
                            direction: Direction.OUT,
                            count: 1
                        });
                        if (enemySafeNodes.length) {
                            minTimer = g.distanceFromToRaw(enemySafeNodes[0].end().get('id'), enemy.Location);
                        }
                    }
                    else {
                        // enemy bomb could be detonated immediately!
                        minTimer = 1;
                    }
                }
                else if (bomb.BombTimer <= minTimer) {
                    // enemy is on bomb, so figure out number of moves
                    let enemySafeNodes = g.closest(g.node(bomb.id), {
                        compare: node => {
                            if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                                return !g.isInBlastRadius(node.get('id'));
                            }
                        },
                        count: 1
                    });
                    minTimer = enemySafeNodes.length > 0 ? enemySafeNodes[0].length() : 2;
                }
            }
            else if (bomb.BombTimer <= minTimer) {
                minBomb = bomb;
                minTimer = Math.max(bomb.BombTimer - 1, 1);
            }
            queue = _.concat(queue, chained);
        }
        return minTimer;
    }
    getChainedBombs(location) {
        let g = this;
        let findChainedBombs = function (bomb) {
            // find bombs that have this bomb in their radius
            let bombs = _(g.nodes('Bomb').query().units())
                .map(node => node.get('entity'))
                .filter(b => _.includes(g.bombBlastNodes[b.id], bomb.id))
                .value();
            return bombs;
        };
        let bombs = [];
        let found = [];
        let queue = [];
        let minTimer = Infinity;
        let minBomb;
        let first = this.getBombsFromBlastLocation(location.toString());
        if (first)
            queue = first;
        while (queue.length) {
            let bomb = queue.pop();
            if (_.includes(found, bomb.id))
                continue;
            let chained = findChainedBombs(bomb);
            if (!chained || chained.length <= 1)
                continue;
            found.push(bomb.id);
            let isOwnBomb = bomb.Owner === g.hero.Key;
            let enemy = g.getPlayer(bomb.Owner);
            let isEnemyOnOwnBomb = enemy.Location.toString() === bomb.Location.toString();
            if (!isOwnBomb) {
                minBomb = bomb;
                if (!isEnemyOnOwnBomb) {
                    if (g.isInOwnBlastRadius(enemy)) {
                        // get enemy dist to closest safe node
                        let enemySafeNodes = g.closest(g.node(enemy.id), {
                            compare: (node) => {
                                if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                                    if (!g.isInBlastRadius(node.get('id'))) {
                                        return true;
                                    }
                                }
                                return false;
                            },
                            direction: Direction.OUT,
                            count: 1
                        });
                        if (enemySafeNodes.length) {
                            minTimer = g.distanceFromToRaw(enemySafeNodes[0].end().get('id'), enemy.Location);
                        }
                    }
                    else {
                        // enemy bomb could be detonated immediately!
                        minTimer = 1;
                    }
                }
                else if (bomb.BombTimer <= minTimer) {
                    // enemy is on bomb, so figure out number of moves
                    let enemySafeNodes = g.closest(g.node(bomb.id), {
                        compare: node => {
                            if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                                return !g.isInBlastRadius(node.get('id'));
                            }
                        },
                        count: 1
                    });
                    minTimer = enemySafeNodes.length > 0 ? enemySafeNodes[0].length() : 2;
                }
            }
            else if (bomb.BombTimer <= minTimer) {
                minBomb = bomb;
                minTimer = Math.max(bomb.BombTimer - 1, 2);
            }
            queue = _.concat(queue, chained);
        }
        return found;
    }
    getThreateningBombs(player) {
        let g = this;
        let bombs = _(this.bombBlastNodes)
            .pickBy(v => _.includes(v, player.id))
            .keys()
            .map(k => _.find(g.bombs, b => b.id == k))
            .value();
        return bombs;
    }
    pathTo(goal, dir) {
        let start = this.hero.id;
        dir = dir === undefined ? 1 : dir;
        return this.pathFromTo(start, goal.toString(), dir);
    }
    pathFromTo(start, goal, dir) {
        let startNode = this.node(start.toString());
        let goalNode = this.node(goal.toString());
        dir = dir === undefined ? 1 : dir;
        if (!startNode || !goalNode)
            return [];
        let pathNodes = this.trace(startNode, goalNode, dir);
        let closerPaths = this.closest(startNode, {
            compare: node => node.get('id') === goalNode.get('id'),
        });
        let path = _(pathNodes._raw)
            .filter((n) => n.constructor.name == 'Node')
            .map((n) => n.get('entity').Location.toString())
            .value();
        return path;
    }
    pathToString(path) {
        return _(path._raw)
            .filter((n) => n.constructor.name == 'Node')
            .map((n) => n.get('entity').Location.toString())
            .value();
    }
    distanceTo(goal, direction) {
        if (goal instanceof Types.Location)
            goal = goal.toString();
        return this.distanceFromTo(this.hero.id, goal, direction);
    }
    distanceFromTo(from, to, direction) {
        let fromNode = this.node(from.toString());
        let toNode = this.node(to.toString());
        if (fromNode === toNode)
            return 0;
        let path = this.trace(fromNode, toNode, direction || 1);
        let dist = Infinity;
        let distCompensate = 0;
        if (path && path.length()) {
            // compensate wieghts for start/end nodes being not walkable
            if (!path.start().get('entity').isWalkable) {
                //path._raw[1].distance = 1
                path._raw.shift();
                path._raw.shift();
                distCompensate++;
            }
            if (!path.end().get('entity').isWalkable) {
                //path._raw[path._raw.length - 2].distance = 1
                path._raw.pop();
                path._raw.pop();
                distCompensate++;
            }
        }
        dist = path.distance() + distCompensate;
        return dist;
    }
    distanceFromToRaw(from, to) {
        let _from = (typeof from === 'string') ? new Types.Location(from) : from;
        let _to = typeof to === 'string' ? new Types.Location(to) : to;
        return Math.ceil(Math.sqrt(Math.pow(_from.X - _to.X, 2) + Math.pow(_from.Y - _to.Y, 2)));
        // let fromNode = this.node(from.toString())
        // let toNode = this.node(to.toString())
        // if (fromNode === toNode) return 0
        // let path: Path = this.trace(fromNode, toNode, 1)
        // if (path && path.length()) {
        // }
        // return path && path.length() ? path.length() : Infinity
    }
    getNodesInRadius(node, radius) {
        let neighbors = [];
        let nStop = false;
        let sStop = false;
        let wStop = false;
        let eStop = false;
        for (var i = 1; i <= radius; i++) {
            let found = [];
            if (!nStop) {
                let n = this.getAt(node.X, node.Y + i);
                if (!n)
                    nStop = true; // undefined means Indestructible wall
                if (n)
                    found.push(n);
                if (n && n.get('entity') instanceof Types.DestructibleWall)
                    nStop = true;
            }
            if (!sStop) {
                let s = this.getAt(node.X, node.Y - i);
                if (!s)
                    sStop = true; // undefined means Indestructible wall
                if (s)
                    found.push(s);
                if (s && s.get('entity') instanceof Types.DestructibleWall)
                    sStop = true;
            }
            if (!wStop) {
                let w = this.getAt(node.X - i, node.Y);
                if (!w)
                    wStop = true; // undefined means Indestructible wall
                if (w)
                    found.push(w);
                if (w && w.get('entity') instanceof Types.DestructibleWall)
                    wStop = true;
            }
            if (!eStop) {
                let e = this.getAt(node.X + i, node.Y);
                if (!e)
                    eStop = true; // undefined means Indestructible wall
                if (e)
                    found.push(e);
                if (e && e.get('entity') instanceof Types.DestructibleWall)
                    eStop = true;
            }
            neighbors = _(found)
                .map((o) => o.get('entity').Location.toString())
                .concat(neighbors)
                .value();
        }
        return neighbors;
    }
    isLocationInRadius(radiusCenter, locationToCheck, radius) {
        let locationsInRadius = this.getNodesInRadius(radiusCenter, radius);
        return (locationsInRadius && _.includes(locationsInRadius, locationToCheck.toString()));
    }
    draw(path) {
        return;
        let lines = '';
        let cols = '   ';
        for (var y = 1; y <= 21; y++) {
            let line = '';
            for (var x = 1; x <= 21; x++) {
                if (y == 1) {
                    cols += _.pad(x.toString(), 4, ' ');
                }
                let entity = this.getEntityAt(x, y);
                let sprite = entity && entity.symbol ? entity.symbol : colors.grey('▓');
                let location = new Types.Location(`${x},${y}`);
                // blast radius
                if (this.isInBlastRadius(location)) {
                    sprite = sprite.bgRed.bold;
                }
                // path to target
                if (path.indexOf(location.toString()) != -1) {
                    sprite = sprite.bgYellow;
                }
                if (entity instanceof Types.Bomb) {
                    let owner = entity.Owner;
                    //sprite = `${sprite}${owner}${sprite}`
                    sprite = _.pad(owner, 3, entity.BombTimer.toString()).toLowerCase();
                }
                else if (entity instanceof Types.Player && entity.Bomb) {
                    sprite = _.pad(entity.Bomb.BombTimer.toString(), 3, entity.Key);
                }
                else {
                    sprite = _.repeat(sprite, 3);
                }
                line += `${sprite} `;
            }
            lines += `${_.padStart(y.toString(), 2, ' ')} ${line}${y}\n`;
        }
        lines = `${cols}\n${lines}${cols}\n`;
        console.log(lines);
    }
}
exports.GameGraph = GameGraph;
//# sourceMappingURL=GameGraph.js.map