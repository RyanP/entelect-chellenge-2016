"use strict"

const colors = require('colors');

//import * as GraphLib from "graphlib"
import { Graph, Node, Edge, Unit, Path } from "./Graph" 
import * as _ from "lodash"
import * as Types from "../Entity/Types"
import { NodeState, EntityFactory } from "../Entity/EntityFactory"
import * as moment from "moment"

export { Node, Edge, Unit, Collection, NodeCollection, EdgeCollection, Query, Path } from "./Graph"

const dirs = [[1, 0], [0, 1], [-1, 0], [0, -1]]

export enum Direction {
  IN = -1,
  ANY = 0,
  OUT = 1
}

export class GameGraph extends Graph {
  public numberOfPlayers: number
  private _bombBlastNodes: Object = {}

  constructor(public playerKey: string, gameState: Types.GameState) {
    super()
    const g = this

    this.playerKey = playerKey
    this.numberOfPlayers = gameState.RegisteredPlayerEntities.length

    this.nodes('Player').createIndex('id').createIndex('key')
    this.nodes('Bomb').createIndex('id')
    this.nodes('Empty').createIndex('id')
    this.nodes('PowerUp').createIndex('id')
    this.nodes('DestructibleWall').createIndex('id')

    // add player nodes first so we can map any existing bombs to them later.
    _.flattenDeep(gameState.GameBlocks)
      .filter((o: NodeState) => o.Entity && o.Entity.$type == "Domain.Entities.PlayerEntity, Domain")
      .forEach((o: NodeState) => {
        let entity = EntityFactory.create(o) as Types.Player
        if (entity.Bomb) {
          g.createNode('Bomb', { id: entity.Bomb.id, entity: entity.Bomb })
        }
        g.createNode(entity.constructor.name, { id: entity.id, key: entity.Key, entity: entity })
      })

    // generate graph
    _(gameState.GameBlocks)
      .flattenDeep()
      .filter((block: NodeState) => {
        if (block.Entity == null) return true
        return block.Entity.$type && block.Entity.$type !== "Domain.Entities.IndestructibleWallEntity, Domain"
      })
      // first add the nodes
      .map((block: NodeState) => {
        let node: Node = g.node(`${block.Location.X},${block.Location.Y}`)
        if (node) return node

        let entity = EntityFactory.create(block)
        let ctor: string
        if (entity.constructor.name == 'SuperPowerUp' || entity.constructor.name == 'BombBagPowerUp' || entity.constructor.name == 'BombRadiusPowerUp') {
          ctor = 'PowerUp'
        }
        return g.createNode(ctor || entity.constructor.name, { id: entity.id, entity: entity })
      })
      // then add the edges for each node
      .map((currentNode) => {
        if (!currentNode) return
        let currentEntity: Types.EntityBase = currentNode.get('entity')
        let neighbourLocations = currentEntity.Location.neighbours

        neighbourLocations.forEach(location => {
          let neighbourNode = g.node(location.toString())
          if (neighbourNode) {
            let neighbourEntity: Types.EntityBase = neighbourNode.get('entity')
            g.createEdge('Path', {from: currentNode.get('id'), to: neighbourNode.get('id')})
              .link(currentNode, neighbourNode)
              .setDistance(1)
          }
        });
      })
      .value() 
    
    let walls = this.nodes('DestructibleWall').query().units()
    _(walls).each((wall: Node) => this.setDestructibleWallWeights(wall))

    let players = this.nodes('Player').query().units()
    _(players).each((player: Node) => this.setPlayerWeights(player))

    let bombs = this.nodes('Bomb').query().units()
    this.addAllBlastNodes(bombs as Node[])
    _(bombs).each((bomb: Node) => this.setBombBlastWeights(bomb)) 
  }

  private setDestructibleWallWeights(wallNode: Node) {
    _(wallNode.inputEdges)
      .each(edge => {
        return edge.setDistance(25)
      })
  }

  private setPlayerWeights(playerNode: Node) {
    _(playerNode.inputEdges)
      .each(edge => edge.setDistance(100))
  }

  private addAllBlastNodes(bombNodes: Node[]) {
    _(bombNodes).each((bombNode: Node) => {
      let bomb = bombNode.get('entity')
      let blastNodes = this.getNodesInRadius(bomb.Location, bomb.BombRadius)
      this._bombBlastNodes[bomb.id] = blastNodes
    })
  }

  private setBombBlastWeights(bombNode: Node) {
    let g = this
    let bomb = bombNode.get('entity')
    let bombOwner = this.getPlayer(bomb.Owner)
    
    // set bomb weight to Infinity
    _(bombNode.inputEdges).each(edge => edge.setDistance(200))
    
    // set weight of all nodes in this bomb's blast radius.
    let blastNodes = this._bombBlastNodes[bomb.id]

    // set weight proportionally to danger of explosion
    let weight = bomb.BombTimer > 2 ? Math.abs(12 - bomb.BombTimer) : 200

    // reset weight if bomb could be detonated by enemy
    if (bombOwner && bombOwner.Key !== g.hero.Key) {
      // is this bomb in a chain
      let bombsInChain = _(blastNodes)
        .filter(blastLocation => g.node(blastLocation).entity === 'Bomb')
        .value()
      
      let minChainedBomb = g.getPlayerBombWithLowestTimer(bombOwner.Key)

      if (bombsInChain.length && bomb.Owner === minChainedBomb.Owner) {
        // is a bomb that could be detonated
        if (!g.isPlayerInBombRadius(g.hero, bomb)) {
          // only if hero is not in the blast radius of this bomb, otherwise
          // this gets in the way of calculating an escape path.
          weight = 150
        } else {
          weight = 1
        }
      }
    }

    _(blastNodes)
      .each(blastNodeId => {
        let blastNode = g.node(blastNodeId)
        let inOwnBlast = this.isLocationInOwnBlastRadius(blastNodeId)
        let heroInOwnBlast = this.isInOwnBlastRadius(g.hero)
        
        // skip bomb nodes as we don't want to override their input weights
        if (blastNode.entity !== 'Empty') {
          return
        }

        _(blastNode.inputEdges).each(edge => {
          // reset weight if we're currently on our own bomb radius so we can escape
          let finalWeight = (inOwnBlast || heroInOwnBlast) && !(weight >= 100) ? 1 : weight

          let oppositeNode = edge.oppositeNode(blastNode)

          if (oppositeNode.entity == 'DestructibleWall') {
            if (!_.includes(blastNodes, oppositeNode.get('id'))) {
              let wallInEdge = g.edges('Path')
                .query().filter({
                  from__is: blastNode.get('id'),
                  to__is: oppositeNode.get('id')
                })
                .first() as Edge
              wallInEdge.setDistance(wallInEdge.distance + 100)
            }
          }
          edge.setDistance(finalWeight)
        })
      })
    
      blastNodes.push(bomb.id)

  }

  getPlayer(key: string): Types.Player {
    let result: Unit = this.nodes('Player').find('key', key)
    return result ? result.get('entity') : undefined
  }

  get players(): Types.Player[] {
    return _(this.nodes('Player').query().units())
      .map(node => node.get('entity') as Types.Player)
      .value()
  }

  get otherPlayers(): Types.Player[] {
    let key = this.playerKey
    let playerNodes: Unit[] = this.nodes('Player').query().exclude({key__is:key}).units()
    return _(playerNodes)
      .map((node: Node) => node.get('entity') as Types.Player)
      .value()
  }

  get hero(): Types.Player {
    return this.getPlayer(this.playerKey)
  }

  get bombs(): Types.Bomb[] {
     return _(this.nodes('Bomb').query().units())
      .map(node => node.get('entity') as Types.Bomb)
      .value()
  }

  get heroBombs(): Types.Bomb[] {
    let hero = this.hero

    let bombs = _(this.nodes('Bomb').query().filter({ entity__Owner__is: hero.Key }).units())
      .map(bomb => bomb.get('entity') as Types.Bomb)
      .value()
    
    return bombs ? bombs : undefined
  }

  public getPlayerBombWithLowestTimer(playerKey: string) {
    if (!playerKey) return undefined
    let bomb = _(this.bombs)
      .filter(b => b.Owner === playerKey && b.BombTimer > 1)
      .minBy((b) => b.BombTimer)
    
    return bomb ? bomb : undefined
  }

  get bombBlastNodes(): Object {
    return this._bombBlastNodes
  }

  get powerUps(): Array<Types.SuperPowerUp | Types.BombRadiusPowerUp | Types.BombBagPowerUp> {
    // return _.filter(this._entities, (o: Node) =>
    //   o.Entity instanceof Types.SuperPowerUp ||
    //   o.Entity instanceof Types.BombRadiusPowerUp ||
    //   o.Entity instanceof Types.BombBagPowerUp
    // )
    //   .map((o: Node) => o.Entity)

    return _(this.nodes('PowerUp').query().units())
      .map(node => node.get('entity'))
      .value()

  }

  public getAt(x: number, y: number)
  public getAt(id: string)
  public getAt(location: Types.Location)
  public getAt(xOrgoal: string | number | Types.Location, y?: number) {
    if (typeof xOrgoal == 'number') {
      return this.node(`${xOrgoal},${y}`)
    } else if (xOrgoal instanceof Types.Location || typeof xOrgoal == "string") {
      return this.node(xOrgoal.toString())
    }
  }

  public getEntityAt(x: number, y: number)
  public getEntityAt(id: string)
  public getEntityAt(location: Types.Location)
  public getEntityAt(xOrgoal: string | number | Types.Location, y?: number) {
    if (typeof xOrgoal == 'number') {
      return this.node(`${xOrgoal},${y}`) ? this.node(`${xOrgoal},${y}`).get('entity') : undefined
    } else if (xOrgoal instanceof Types.Location || typeof xOrgoal == "string") {
      return this.node(xOrgoal.toString()) ? this.node(xOrgoal.toString()).get('entity') : undefined
    }
  }

  public isInBlastRadius(id: string): boolean
  public isInBlastRadius(location: Types.Location): boolean
  public isInBlastRadius(node: Node): boolean
  public isInBlastRadius(player: Types.Player): boolean
  public isInBlastRadius(idOrLocationOrNodeOrPlayer: string | Types.Location | Node | Types.Player): boolean {
    let _id: string
    
    if (typeof idOrLocationOrNodeOrPlayer === 'string') _id = idOrLocationOrNodeOrPlayer as string
    if (idOrLocationOrNodeOrPlayer instanceof Types.Location) _id = idOrLocationOrNodeOrPlayer.toString()
    if (idOrLocationOrNodeOrPlayer.constructor == Node) _id = (idOrLocationOrNodeOrPlayer as Node).get('entity').id
    if (idOrLocationOrNodeOrPlayer.constructor == Types.Player) _id = (idOrLocationOrNodeOrPlayer as Types.Player).id

    let result = _(this.bombBlastNodes).values().flatten().includes(_id)

    return result ? true : false
  }

  public isInOwnBlastRadius(player: Types.Player): boolean {
    let bombs = this.getBombsFromBlastLocation(player.id)
    if (bombs) {
      bombs = _.filter(bombs, bomb => bomb.Owner === player.Key)
    }
    return bombs && bombs.length > 0
  }

  public isLocationInOwnBlastRadius(location: string | Types.Location) {
    let g = this
    let bombs = this.getBombsFromBlastLocation(location)
    if (bombs) {
      bombs = _.filter(bombs, bomb => bomb.Owner === g.hero.Key)
    }
    return bombs && bombs.length > 0
  }

  public isPlayerInBombRadius(player: Types.Player, bomb: Types.Bomb) {
    let playerBombIds = _(this.bombBlastNodes[bomb.id])
    return _(playerBombIds).includes(player.id)
  }

  public getBombsFromBlastLocation(location: string | Types.Location): Types.Bomb[] {
    let g = this
    let bomb = this.node(location.toString())
    if (bomb && bomb.entity === 'Bomb') return [bomb.get('entity')]

    let result = _(this.nodes('Bomb').query().units())
      .map(node => node.get('entity') as Types.Bomb)
      .filter(b => _.includes(this.bombBlastNodes[b.id], location.toString()))
      .value()
    
    return result && result.length ? result : undefined
  }

  public getChainedBombsMinTimer(location: string | Types.Location) {
    let g = this
    
    let findChainedBombs = function (bomb: Types.Bomb): Types.Bomb[] {
      // find bombs that have this bomb in their radius
      let bombs = _(g.nodes('Bomb').query().units())
      .map(node => node.get('entity') as Types.Bomb)
      .filter(b => _.includes(g.bombBlastNodes[b.id], bomb.id))
      .value()
      
      return bombs as Types.Bomb[]
    }

    let bombs: Types.Bomb[] = []
    let found: string[] = []
    let queue: Types.Bomb[] = []
    let minTimer = Infinity
    let minBomb: Types.Bomb

    let first = this.getBombsFromBlastLocation(location.toString())
    if (first) queue = first

    while (queue.length) {
      let bomb = queue.pop()

      if (_.includes(found, bomb.id)) continue

      let chained = findChainedBombs(bomb)

      if (!chained || chained.length === 1) minTimer = bomb.BombTimer

      found.push(bomb.id)

      let isOwnBomb = bomb.Owner === g.hero.Key
      let enemy = g.getPlayer(bomb.Owner)
      let isEnemyOnOwnBomb = enemy.Location.toString() === bomb.Location.toString()

      if (!isOwnBomb) {
        minBomb = bomb
        if (!isEnemyOnOwnBomb) {
          if (g.isInOwnBlastRadius(enemy)) {
            // get enemy dist to closest safe node
            let enemySafeNodes: Path[] = g.closest(g.node(enemy.id),{
              compare: (node: Node) => {
                if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                  if (!g.isInBlastRadius(node.get('id'))) {
                    return true
                  }
                }
                return false
              },
              direction: Direction.OUT,
              count: 1
            })

            if (enemySafeNodes.length) {
              minTimer = g.distanceFromToRaw(enemySafeNodes[0].end().get('id'), enemy.Location)
            }
          } else {
            // enemy bomb could be detonated immediately!
            minTimer = 1
          }
        } else if (bomb.BombTimer <= minTimer) {
          // enemy is on bomb, so figure out number of moves
          let enemySafeNodes = g.closest(g.node(bomb.id), {
            compare: node => {
              if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                return !g.isInBlastRadius(node.get('id'))
              }
            },
            count: 1
          })

          minTimer = enemySafeNodes.length > 0 ? enemySafeNodes[0].length() : 2
        }
      } else if (bomb.BombTimer <= minTimer) {
        minBomb = bomb
        minTimer = Math.max(bomb.BombTimer - 1, 1)
      }
      queue = _.concat(queue, chained)
    }
    
    return minTimer
  }

  public getChainedBombs(location: string | Types.Location) {
    let g = this
    
    let findChainedBombs = function (bomb: Types.Bomb): Types.Bomb[] {
      // find bombs that have this bomb in their radius
      let bombs = _(g.nodes('Bomb').query().units())
      .map(node => node.get('entity') as Types.Bomb)
      .filter(b => _.includes(g.bombBlastNodes[b.id], bomb.id))
      .value()
      
      return bombs as Types.Bomb[]
    }

    let bombs: Types.Bomb[] = []
    let found: string[] = []
    let queue: Types.Bomb[] = []
    let minTimer = Infinity
    let minBomb: Types.Bomb

    let first = this.getBombsFromBlastLocation(location.toString())
    if (first) queue = first

    while (queue.length) {
      let bomb = queue.pop()

      if (_.includes(found, bomb.id)) continue

      let chained = findChainedBombs(bomb)

      if (!chained || chained.length <= 1) continue

      found.push(bomb.id)

      let isOwnBomb = bomb.Owner === g.hero.Key
      let enemy = g.getPlayer(bomb.Owner)
      let isEnemyOnOwnBomb = enemy.Location.toString() === bomb.Location.toString()

      if (!isOwnBomb) {
        minBomb = bomb
        if (!isEnemyOnOwnBomb) {
          if (g.isInOwnBlastRadius(enemy)) {
            // get enemy dist to closest safe node
            let enemySafeNodes: Path[] = g.closest(g.node(enemy.id),{
              compare: (node: Node) => {
                if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                  if (!g.isInBlastRadius(node.get('id'))) {
                    return true
                  }
                }
                return false
              },
              direction: Direction.OUT,
              count: 1
            })

            if (enemySafeNodes.length) {
              minTimer = g.distanceFromToRaw(enemySafeNodes[0].end().get('id'), enemy.Location)
            }
          } else {
            // enemy bomb could be detonated immediately!
            minTimer = 1
          }
        } else if (bomb.BombTimer <= minTimer) {
          // enemy is on bomb, so figure out number of moves
          let enemySafeNodes = g.closest(g.node(bomb.id), {
            compare: node => {
              if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                return !g.isInBlastRadius(node.get('id'))
              }
            },
            count: 1
          })

          minTimer = enemySafeNodes.length > 0 ? enemySafeNodes[0].length() : 2
        }
      } else if (bomb.BombTimer <= minTimer) {
        minBomb = bomb
        minTimer = Math.max(bomb.BombTimer - 1, 2)
      }
      queue = _.concat(queue, chained)
    }
    
    return found
  }

  public getThreateningBombs(player: Types.Player): Types.Bomb[] {
    let g = this
    let bombs = _(this.bombBlastNodes)
      .pickBy(v => _.includes(v, player.id))
      .keys()
      .map(k => _.find(g.bombs, b => b.id == k))
      .value()
    
    return bombs
  }

  public pathTo(goal: string | Types.Location, dir?: Direction) : string[] {
    let start = this.hero.id
    dir = dir === undefined ? 1 : dir
    return this.pathFromTo(start, goal.toString(), dir)
  }

  public pathFromTo(start: string | Types.Location, goal: string | Types.Location, dir?: Direction): string[] {
    let startNode = this.node(start.toString())
    let goalNode = this.node(goal.toString())
    dir = dir === undefined ? 1 : dir

    if (!startNode || !goalNode) return []

    let pathNodes = this.trace(startNode, goalNode, dir);

    let closerPaths = this.closest(startNode, {
      compare: node => node.get('id') === goalNode.get('id'),
    })

    let path = _(pathNodes._raw)
      .filter((n: Unit) => n.constructor.name == 'Node')
      .map((n: Node) => n.get('entity').Location.toString())
      .value()

    return path
  }

  public pathToString(path: Path): string[] {
    return _(path._raw)
      .filter((n: Unit) => n.constructor.name == 'Node')
      .map((n: Node) => n.get('entity').Location.toString())
      .value()
  }

  public distanceTo(goal: string | Types.Location, direction?: number): number {
    if (goal instanceof Types.Location) goal = goal.toString()
    return this.distanceFromTo(this.hero.id, goal as string, direction)
  }

  public distanceFromTo(from: string | Types.Location, to: string | Types.Location, direction?: number): number {
    let fromNode = this.node(from.toString())
    let toNode = this.node(to.toString())
    if (fromNode === toNode) return 0
    let path: Path = this.trace(fromNode, toNode, direction || 1)
    let dist = Infinity
    let distCompensate = 0
    if (path && path.length()) {
      // compensate wieghts for start/end nodes being not walkable
      if (!path.start().get('entity').isWalkable) {
        //path._raw[1].distance = 1
        path._raw.shift()
        path._raw.shift()
        distCompensate ++
      } 
      if (!path.end().get('entity').isWalkable) {
        //path._raw[path._raw.length - 2].distance = 1
        path._raw.pop()
        path._raw.pop()
        distCompensate ++
      }
    }
    dist = path.distance() + distCompensate
    return dist
  }

  public distanceFromToRaw(from: string | Types.Location, to: string | Types.Location) {
    let _from = (typeof from === 'string') ? new Types.Location(from as string) : from
    let _to = typeof to === 'string' ? new Types.Location(to as string) : to
    return Math.ceil(Math.sqrt(Math.pow(_from.X - _to.X, 2) + Math.pow(_from.Y - _to.Y, 2)))
    
    // let fromNode = this.node(from.toString())
    // let toNode = this.node(to.toString())
    // if (fromNode === toNode) return 0
    // let path: Path = this.trace(fromNode, toNode, 1)
    // if (path && path.length()) {
      
    // }
    // return path && path.length() ? path.length() : Infinity
  }

  public getNodesInRadius(node: Types.Location, radius: number): string[] {
    let neighbors: string[] = []
    let nStop = false
    let sStop = false
    let wStop = false
    let eStop = false

    for (var i = 1; i <= radius; i++) {
      let found: Node[] = []
      if (!nStop) {
        let n = this.getAt(node.X, node.Y + i)
        if (!n) nStop = true // undefined means Indestructible wall
        if (n) found.push(n)
        if (n && n.get('entity') instanceof Types.DestructibleWall) nStop = true
      }

      if (!sStop) {
        let s = this.getAt(node.X, node.Y - i)
        if (!s) sStop = true // undefined means Indestructible wall
        if (s) found.push(s)
        if (s && s.get('entity') instanceof Types.DestructibleWall) sStop = true
      }

      if (!wStop) {
        let w = this.getAt(node.X - i, node.Y)
        if (!w) wStop = true // undefined means Indestructible wall
        if (w) found.push(w)
        if (w && w.get('entity') instanceof Types.DestructibleWall) wStop = true
      }

      if (!eStop) {
        let e = this.getAt(node.X + i, node.Y)
        if (!e) eStop = true // undefined means Indestructible wall
        if (e) found.push(e)
        if (e && e.get('entity') instanceof Types.DestructibleWall) eStop = true
      }

      neighbors = _(found)
        .map((o) => o.get('entity').Location.toString())
        .concat(neighbors)
        .value()
    }
    return neighbors
  }

  public isLocationInRadius(radiusCenter: Types.Location, locationToCheck: Types.Location, radius: number): boolean {
    let locationsInRadius = this.getNodesInRadius(radiusCenter, radius)
    return (locationsInRadius && _.includes(locationsInRadius, locationToCheck.toString())) 
  }

  public draw(path: string[]) {
    return
    let lines = ''
    let cols = '   '

    for (var y = 1; y <= 21; y++) {
      let line = ''
      for (var x = 1; x <= 21; x++) {
        if (y == 1) {
          cols += _.pad(x.toString(), 4, ' ')
        }

        let entity: Types.IEntity = this.getEntityAt(x, y)
        let sprite = entity && entity.symbol ? entity.symbol : colors.grey('▓')
        let location: Types.Location = new Types.Location(`${x},${y}`)

        // blast radius
        if (this.isInBlastRadius(location)) {
          sprite = sprite.bgRed.bold
        }

        // path to target
        if (path.indexOf(location.toString()) != -1) {
          sprite = sprite.bgYellow
        }

        if (entity instanceof Types.Bomb) {
          let owner = entity.Owner as string
          //sprite = `${sprite}${owner}${sprite}`
          sprite = _.pad(owner, 3, entity.BombTimer.toString()).toLowerCase()
        } else if (entity instanceof Types.Player && entity.Bomb) {
          sprite = _.pad(entity.Bomb.BombTimer.toString(), 3, entity.Key)
        } else {        
          sprite = _.repeat(sprite, 3)
        }
        
        line += `${sprite} `
      }
      lines += `${_.padStart(y.toString(), 2, ' ')} ${line}${y}\n`
    }
    lines = `${cols}\n${lines}${cols}\n`
    console.log(lines)
  }

}