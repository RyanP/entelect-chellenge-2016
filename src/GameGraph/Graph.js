"use strict";
const ug_1 = require("ug");
const _ = require("lodash");
const Path = require('../../node_modules/ug/lib/path.js');
var ug_2 = require("ug");
exports.Node = ug_2.Node;
exports.Edge = ug_2.Edge;
exports.Unit = ug_2.Unit;
exports.Collection = ug_2.Collection;
exports.NodeCollection = ug_2.NodeCollection;
exports.EdgeCollection = ug_2.EdgeCollection;
exports.Query = ug_2.Query;
exports.Path = ug_2.Path;
// Path.prototype.distance = function () {
//   return this._raw.filter(function (v, i) {
//     return !!(i & 1);
//   }).reduce(function (p, c) {
//     return p + c.distance;
//   }, 0);
// };
class Graph extends ug_1.Graph {
    constructor() {
        super();
    }
    node(id) {
        return _(this._nodes)
            .find(node => {
            return node.get('id') === id;
        });
    }
    get edgeCollections() {
        return this._edgeCollections;
    }
    get nodeCollections() {
        return this._nodeCollections;
    }
    closest(node, opts) {
        opts = opts || {};
        return this._search(node, opts.compare, opts.count, opts.direction, opts.minDepth, opts.maxDepth);
    }
    trace(fromNode, toNode, direction) {
        let passCondition = function (node) {
            return node === toNode;
        };
        return this._search(fromNode, passCondition, 1, direction)[0] || new Path([]);
    }
    _search(node, passCondition, count, direction, minDepth, maxDepth) {
        passCondition = (typeof passCondition === 'function') ? passCondition : function (node) {
            return true;
        };
        direction |= 0;
        count = Math.max(0, count | 0);
        minDepth = Math.max(0, minDepth | 0);
        maxDepth = Math.max(0, maxDepth | 0);
        let nodePath = Object.create(null);
        nodePath[node.__uniqid__] = [node];
        let depthMap = new Map();
        depthMap.set(0, [node]);
        let depthList = [0];
        let found = [];
        let getPath = this._getPath;
        function enqueue(node, depth) {
            depthMap.has(depth) ?
                depthMap.get(depth).push(node) :
                depthMap.set(depth, [node]);
            orderedSetInsert(depthList, depth);
        }
        function orderedSetInsert(arr, val) {
            // fix bug in original library by using lodash for orderend insert
            return arr.splice(_.sortedIndex(arr, val), 0, val);
        }
        function readNode(node, curDepth) {
            let edges = (direction === 0 ? node.edges : direction > 0 ? node.outputEdges : node.inputEdges);
            for (let i = 0, len = edges.length; i < len; i++) {
                let edge = edges[i];
                let depth = curDepth + edge.distance;
                // filter out nodes with a distance greater that max depth
                // or with an Infinate depth only if they're not the 1st or
                // last nodes 
                //if ((maxDepth && depth > maxDepth)) {      
                if ((maxDepth && depth > maxDepth)) {
                    continue;
                }
                let tnode = edges[i].oppositeNode(node);
                if (!nodePath[tnode.__uniqid__]) {
                    nodePath[tnode.__uniqid__] = [edge, tnode];
                    enqueue(tnode, depth);
                }
            }
            if (curDepth >= minDepth && passCondition(node)) {
                return new Path(getPath(node, nodePath));
            }
            return false;
        }
        while (depthList.length) {
            let curDepth = depthList.shift();
            let queue = depthMap.get(curDepth);
            while (queue.length) {
                let path = readNode(queue.shift(), curDepth);
                path && found.push(path);
                if (count && found.length >= count) {
                    return found;
                }
            }
        }
        return found;
    }
}
exports.Graph = Graph;
//# sourceMappingURL=Graph.js.map