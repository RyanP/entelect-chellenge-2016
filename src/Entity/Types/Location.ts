"use strict"

import * as _ from "lodash"

export interface LocationState {
  X: number
  Y: number
  dist?: number
}

export class Location {
  public X: number
  public Y: number
  public dist: number

  constructor(id: string, dist?: number)
  constructor(X: number, Y: number, dist?: number)
  constructor(xOrId: number | string, yOrDist?: number, dist?: number) {
    switch (typeof xOrId) {
      case 'string':
        let location = (xOrId as string).split(',') as string[]
        this.X = parseInt(location[0])
        this.Y = parseInt(location[1])
        this.dist = yOrDist
        break
      case 'number':
        this.X = xOrId as number
        this.Y = yOrDist as number
        this.dist = dist
        break
    }
  }

  public get up(): Location {
    return new Location(this.X, this.Y + 1)
  }

  public get down(): Location {
    return new Location(this.X, this.Y - 1)
  }

  public get left(): Location {
    return new Location(this.X - 1, this.Y)
  }

  public get right(): Location {
    return new Location(this.X + 1, this.Y)
  }

  public get neighbours(): Location[] {
    return [this.left, this.up, this.right, this.down]
  }

  public toString() {
    return `${this.X},${this.Y}`
  }
}