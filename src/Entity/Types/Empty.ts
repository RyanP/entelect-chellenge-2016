"use strict"

const colors = require('colors');
import { EntityBase, EntityState, IEntity } from "./index"

export class Empty extends EntityBase implements IEntity {
    constructor(state: EntityState) {
        super(state)
    }
}