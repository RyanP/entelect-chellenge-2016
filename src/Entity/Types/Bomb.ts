'use strict'

const colors = require('colors');
import { EntityBase, EntityState, Player, PlayerState, IEntity } from "./"

export interface BombState extends EntityState {
  IsExploding: boolean
  BombRadius: number
  BombTimer: number
  Owner: PlayerState
}

export class Bomb extends EntityBase implements IEntity {
  public static bombs: Bomb[]

  public BombRadius: number 
  public BombTimer: number
  public IsExploding: boolean
  public Owner: string

  constructor(state: BombState) {
    super(state)

    this.BombRadius = state.BombRadius
    this.BombTimer = state.BombTimer
    this.IsExploding = state.IsExploding
    this.Owner = state.Owner.Key
  }

  public get symbol(): string { return colors.white.bold(this.BombTimer.toString()) }
}