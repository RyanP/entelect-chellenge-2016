"use strict"

const colors = require('colors');
import { EntityBase, EntityState, IEntity } from "./"

export class IndestructibleWall extends EntityBase implements IEntity {
    constructor(state: EntityState) {
        super(state)
    }
    public get symbol(): string { return colors.grey('▓') } 
}