"use strict"

const colors = require('colors');
import { EntityBase, EntityState, IPowerUp } from "./"

export class SuperPowerUp extends EntityBase implements IPowerUp {
    constructor(state: EntityState) {
        super(state)
    }

    public get value(): number { return 100 }
    public get symbol(): string { return colors.blue.bold('$') }
}