"use strict";
const colors = require('colors');
const _1 = require("./");
class SuperPowerUp extends _1.EntityBase {
    constructor(state) {
        super(state);
    }
    get value() { return 100; }
    get symbol() { return colors.blue.bold('$'); }
}
exports.SuperPowerUp = SuperPowerUp;
//# sourceMappingURL=SuperPowerUp.js.map