"use strict";
const colors = require('colors');
const _1 = require("./");
class BombRadiusPowerUp extends _1.EntityBase {
    constructor(state) {
        super(state);
    }
    get value() { return 50; }
    get symbol() { return colors.blue.bold('!'); }
}
exports.BombRadiusPowerUp = BombRadiusPowerUp;
//# sourceMappingURL=BombRadiusPowerUp.js.map