'use strict';
const Entity = require('./');
const index_1 = require("./index");
const _ = require("lodash");
class EntityBase {
    constructor(state) {
        this._location = new index_1.Location(state.Location.X, state.Location.Y);
    }
    get id() { return this._location.toString(); }
    get Location() { return this._location; }
    set Location(v) { this._location = v; }
    get value() { return 0; }
    get symbol() { return ' '; }
    get isDestructible() { return this instanceof Entity.DestructibleWall || this instanceof Entity.Player || this instanceof Entity.Bomb; }
    get isDestructibleWall() { return this instanceof Entity.DestructibleWall; }
    get isIndestructible() { return this instanceof Entity.IndestructibleWall; }
    get isBomb() { return this instanceof Entity.Bomb; }
    get isPlayer() { return this instanceof Entity.Player; }
    get isEmpty() { return this instanceof Entity.Empty; }
    get isSuperPowerUp() { return this instanceof Entity.SuperPowerUp; }
    get isBombBagPowerUp() { return this instanceof Entity.BombBagPowerUp; }
    get isBombRadiusPowerUp() { return this instanceof Entity.BombRadiusPowerUp; }
    get isPowerUp() { return this instanceof Entity.SuperPowerUp || this instanceof Entity.BombBagPowerUp || this instanceof Entity.BombRadiusPowerUp; }
    get isWalkable() { return this.isEmpty || this.isPowerUp; }
    toString() {
        return `${this._location.X},${this._location.X} | <${this.constructor.name}>`;
    }
}
exports.EntityBase = EntityBase;
//# sourceMappingURL=EntityBase.js.map