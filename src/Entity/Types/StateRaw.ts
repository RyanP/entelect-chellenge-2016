"use strict"

import { LocationState } from "./"

export interface State {
  Location: LocationState
}

export interface RegisteredPlayerEntity {
  Name: string;
  Key: string;
  Points: number;
  Killed: boolean;
  BombBag: number;
  BombRadius: number;
  Location: Location;
}

export interface GameState {
  RegisteredPlayerEntities: RegisteredPlayerEntity[];
  CurrentRound: number;
  PlayerBounty: number;
  MapHeight: number;
  MapWidth: number;
  GameBlocks: any[][];
  MapSeed: number;
}



// Entity?: any
// Bomb?: any
// PowerUp?: any
// Name?: string
// Key?: string
// Points?: number
// Killed?: boolean 
// BombBag?: number
// BombRadius?: number
// BombTimer?: number
// Owner?: any
// IsExploding?: boolean
// Location: any