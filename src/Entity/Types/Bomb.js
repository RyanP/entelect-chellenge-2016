'use strict';
const colors = require('colors');
const _1 = require("./");
class Bomb extends _1.EntityBase {
    constructor(state) {
        super(state);
        this.BombRadius = state.BombRadius;
        this.BombTimer = state.BombTimer;
        this.IsExploding = state.IsExploding;
        this.Owner = state.Owner.Key;
    }
    get symbol() { return colors.white.bold(this.BombTimer.toString()); }
}
exports.Bomb = Bomb;
//# sourceMappingURL=Bomb.js.map