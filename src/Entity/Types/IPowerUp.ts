"use strict"

import { IEntity } from './'

export interface IPowerUp extends IEntity{
  value: number
  symbol: string
}