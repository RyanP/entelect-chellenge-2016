"use strict";
const colors = require('colors');
const _1 = require("./");
class IndestructibleWall extends _1.EntityBase {
    constructor(state) {
        super(state);
    }
    get symbol() { return colors.grey('▓'); }
}
exports.IndestructibleWall = IndestructibleWall;
//# sourceMappingURL=IndestructibleWall.js.map