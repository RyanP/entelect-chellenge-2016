"use strict";
class Location {
    constructor(xOrId, yOrDist, dist) {
        switch (typeof xOrId) {
            case 'string':
                let location = xOrId.split(',');
                this.X = parseInt(location[0]);
                this.Y = parseInt(location[1]);
                this.dist = yOrDist;
                break;
            case 'number':
                this.X = xOrId;
                this.Y = yOrDist;
                this.dist = dist;
                break;
        }
    }
    get up() {
        return new Location(this.X, this.Y + 1);
    }
    get down() {
        return new Location(this.X, this.Y - 1);
    }
    get left() {
        return new Location(this.X - 1, this.Y);
    }
    get right() {
        return new Location(this.X + 1, this.Y);
    }
    get neighbours() {
        return [this.left, this.up, this.right, this.down];
    }
    toString() {
        return `${this.X},${this.Y}`;
    }
}
exports.Location = Location;
//# sourceMappingURL=Location.js.map