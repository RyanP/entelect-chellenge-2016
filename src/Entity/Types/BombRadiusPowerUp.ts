"use strict"

const colors = require('colors');
import { EntityBase, EntityState, IPowerUp } from "./"

export class BombRadiusPowerUp extends EntityBase implements IPowerUp {
    constructor(state: EntityState) {
        super(state)
    }

    public get value(): number { return 50 }
    public get symbol(): string { return colors.blue.bold('!') }
}