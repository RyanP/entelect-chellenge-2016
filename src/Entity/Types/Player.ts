"use strict"

import { Bomb } from './Bomb'

const colors = require('colors');
import { State, EntityBase, EntityState, Location, LocationState, IEntity } from "./"

export interface PlayerState extends EntityState {
  Name: string
  Key: string
  Points: number
  Killed: boolean
  Bomb: Bomb
  BombBag: number
  BombRadius: number
}

export class Player extends EntityBase implements IEntity {
  public Name: string
  public Key: string
  public Points: number
  public Killed: boolean
  public Bomb: Bomb
  public BombBag: number
  public BombRadius: number

  constructor(state: PlayerState) {
    super(state)

    this.Name = state.Name
    this.Key = state.Key
    this.Points = state.Points
    this.Killed = state.Killed
    this.BombBag = state.BombBag
    this.BombRadius = state.BombRadius
  }

  public get value(): number { return 100 }
  public get symbol(): string {
    let symbol = this.Bomb ?  this.Key.toLowerCase() : this.Key
    return colors.blue.bold(symbol)
  }
}