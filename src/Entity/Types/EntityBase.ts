'use strict'

import * as Entity from './'
import { State, Location, LocationState } from "./index" 
const _ = require("lodash")

export type RawEntityType = "Domain.Entities.Player, Domain" |
  "Domain.Entities.BombEntity, Domain" |
  "Domain.Entities.IndestructibleWallEntity, Domain" |
  "Domain.Entities.DestructibleWallEntity, Domain" |
  "Domain.Entities.PowerUps.SuperPowerUp, Domain" |
  "Domain.Entities.PowerUps.BombBagPowerUp, Domain" |
  "Domain.Entities.PowerUps.BombRadiusPowerUp, Domain"

export interface EntityState extends State {
    $type?: RawEntityType
} 

export interface IEntity extends State {
    id: string
    Location: Location
    value: number
    symbol: string
    isDestructible: boolean
    isDestructibleWall: boolean
    isIndestructible: boolean
    isBomb: boolean
    isPlayer: boolean
    isEmpty: boolean
    isSuperPowerUp: boolean
    isBombBagPowerUp: boolean
    isBombRadiusPowerUp: boolean
    isPowerUp: boolean
    isWalkable: boolean
    toString(): string
} 

export class EntityBase implements IEntity {
    private _location: Location

    constructor(state: EntityState) {
        this._location = new Location(state.Location.X, state.Location.Y)
    }

    public get id():string { return this._location.toString() }    
    public get Location(): Location { return this._location }
    public set Location(v : Location) { this._location = v }
    public get value(): number { return 0 }
    public get symbol(): string { return ' ' } 
    
    public get isDestructible(): boolean { return this instanceof Entity.DestructibleWall || this instanceof Entity.Player || this instanceof Entity.Bomb }
    public get isDestructibleWall(): boolean { return this instanceof Entity.DestructibleWall }
    public get isIndestructible(): boolean { return this instanceof Entity.IndestructibleWall }
    public get isBomb(): boolean { return this instanceof Entity.Bomb }
    public get isPlayer(): boolean { return this instanceof Entity.Player }
    public get isEmpty(): boolean { return this instanceof Entity.Empty }
    public get isSuperPowerUp(): boolean { return this instanceof Entity.SuperPowerUp }
    public get isBombBagPowerUp(): boolean { return this instanceof Entity.BombBagPowerUp }
    public get isBombRadiusPowerUp(): boolean { return this instanceof Entity.BombRadiusPowerUp }
    public get isPowerUp(): boolean { return this instanceof Entity.SuperPowerUp || this instanceof Entity.BombBagPowerUp || this instanceof Entity.BombRadiusPowerUp }
    public get isWalkable(): boolean { return this.isEmpty || this.isPowerUp }

    public toString() : string {
        return `${this._location.X},${this._location.X} | <${this.constructor.name}>` 
    }
}