"use strict";
const colors = require('colors');
const _1 = require("./");
class Player extends _1.EntityBase {
    constructor(state) {
        super(state);
        this.Name = state.Name;
        this.Key = state.Key;
        this.Points = state.Points;
        this.Killed = state.Killed;
        this.BombBag = state.BombBag;
        this.BombRadius = state.BombRadius;
    }
    get value() { return 100; }
    get symbol() {
        let symbol = this.Bomb ? this.Key.toLowerCase() : this.Key;
        return colors.blue.bold(symbol);
    }
}
exports.Player = Player;
//# sourceMappingURL=Player.js.map