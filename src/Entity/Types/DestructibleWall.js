"use strict";
const colors = require('colors');
const _1 = require("./");
class DestructibleWall extends _1.EntityBase {
    constructor(state) {
        super(state);
    }
    get value() { return 30; }
    get symbol() { return colors.grey('░'); }
}
exports.DestructibleWall = DestructibleWall;
//# sourceMappingURL=DestructibleWall.js.map