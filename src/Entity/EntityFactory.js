"use strict";
const Types = require("./Types");
class EntityFactory {
    constructor() {
        throw new Error("Cannot new this class");
    }
    static create(state) {
        // For some reason Bombs don't actually have the $type property in the 
        // raw state file. 
        if (state.Bomb)
            state.Bomb.$type = 'Domain.Entities.BombEntity, Domain';
        let entityState = state.Entity || state.PowerUp || state.Bomb;
        let name = entityState ? EntityFactory.parseTypeString(entityState.$type) : 'Empty';
        let type = null;
        switch (name) {
            case 'Empty':
                type = Types.Empty;
                break;
            case 'Player':
                type = Types.Player;
                break;
            case 'Bomb':
                type = Types.Bomb;
                break;
            case 'IndestructibleWall':
                type = Types.IndestructibleWall;
                break;
            case 'DestructibleWall':
                type = Types.DestructibleWall;
                break;
            case 'SuperPowerUp':
                type = Types.SuperPowerUp;
                break;
            case 'BombBagPowerUp':
                type = Types.BombBagPowerUp;
                break;
            case 'BombRaduisPowerUp':
                type = Types.BombRadiusPowerUp;
                break; // note the spelling mistake :)
        }
        let entity = new type(entityState || state);
        if (name == 'Player' && state.Bomb) {
            entity.Bomb = new Types.Bomb(state.Bomb);
        }
        return entity;
    }
    static parseTypeString(type) {
        let s = type.split(',')[0].split('.');
        return s[s.length - 1].replace('Entity', '');
    }
}
exports.EntityFactory = EntityFactory;
//# sourceMappingURL=EntityFactory.js.map