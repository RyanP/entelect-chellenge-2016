import _ from 'lodash'
import * as Types from "./Types"

export interface NodeState extends Types.State {
  Entity: Types.EntityState
  Bomb: Types.BombState
  PowerUp: any
  IsExploding: boolean
}

export class EntityFactory {

  constructor() {
    throw new Error("Cannot new this class")
  }

  public static create(state: NodeState): Types.EntityBase {
    // For some reason Bombs don't actually have the $type property in the 
    // raw state file. 
    if (state.Bomb) state.Bomb.$type = 'Domain.Entities.BombEntity, Domain'

    let entityState = state.Entity || state.PowerUp || state.Bomb

    let name = entityState ? EntityFactory.parseTypeString(entityState.$type) : 'Empty'
    let type = null

    switch (name) {
      case 'Empty': type = Types.Empty; break
      case 'Player': type = Types.Player; break
      case 'Bomb': type = Types.Bomb; break
      case 'IndestructibleWall': type = Types.IndestructibleWall; break
      case 'DestructibleWall': type = Types.DestructibleWall; break
      case 'SuperPowerUp': type = Types.SuperPowerUp; break
      case 'BombBagPowerUp': type = Types.BombBagPowerUp; break
      case 'BombRaduisPowerUp': type = Types.BombRadiusPowerUp; break // note the spelling mistake :)
    }

    let entity = new type(entityState || state)

    if (name == 'Player' && state.Bomb) {
      (entity as Types.Player).Bomb = new Types.Bomb(state.Bomb);
    }

    return entity
  }

  private static parseTypeString(type: string): string | boolean {
    let s = type.split(',')[0].split('.')
    return s[s.length - 1].replace('Entity', '')
  }
}