import { BehaviorTree, BlackBoard } from "../BT"
import * as NodeTypes from './Node'
import { GameGraph } from '../GameGraph'
import { Player } from '../Entity'
import { Context, MOVE } from './'
import { treeData } from './treeData'

export class AI { 
  private brain: BehaviorTree
  private context: Context

  constructor(private _graph: GameGraph) {
    this.brain = new BehaviorTree()
    this.brain.load(treeData, NodeTypes)
    this.context = new Context(_graph, treeData.id)
  }

  public getMove(): MOVE {
    this.brain.tick(this._graph.hero, this.context)

    let path = this.context.get('goalPath')
    let move = this.context.get('move')

    this._graph.draw(path || [])

    return move
  }

}
