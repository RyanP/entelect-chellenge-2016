export const treeData = {
  "id": "80fb54df-039d-40ad-82bf-aa6d5b68b652",
  "title": "Behave",
  "description": "",
  "root": "a5c5325e-7656-4aba-972c-5fc882f3bd88",
  "properties": {},
  "nodes": {
    "1024e1c4-01a0-4b0e-8469-a8e641d851f4": {
      "id": "1024e1c4-01a0-4b0e-8469-a8e641d851f4",
      "name": "IsInBlastRadius",
      "title": "In Blast Radius?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -2520
      }
    },
    "adbdd1e3-c626-4933-8738-c00ee6dc11ee": {
      "id": "adbdd1e3-c626-4933-8738-c00ee6dc11ee",
      "name": "IsOnOwnBomb",
      "title": "On own Bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -2616
      }
    },
    "4a67b6e2-7768-47cb-8dc4-55dec887c311": {
      "id": "4a67b6e2-7768-47cb-8dc4-55dec887c311",
      "name": "FindSafeLocation",
      "title": "Find Safe Location",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -2088
      }
    },
    "44f9d2dd-7149-4e4d-8f7f-404bba18336b": {
      "id": "44f9d2dd-7149-4e4d-8f7f-404bba18336b",
      "name": "MoveTo",
      "title": "Move to Safe Location",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -1824
      }
    },
    "81f83b43-941f-43f9-8df2-417aaf79308b": {
      "id": "81f83b43-941f-43f9-8df2-417aaf79308b",
      "name": "Sequence",
      "title": "CollectPowerUp",
      "description": "",
      "properties": {},
      "display": {
        "x": -216,
        "y": -876
      },
      "children": [
        "3946da4e-c994-4e00-8605-2222424ca5ed",
        "92dba9b4-fe52-4aed-b46b-8fe7909a3610",
        "939a2eb1-56de-48f3-82f7-cafb47348816",
        "0c0bdfdf-f6b4-43c5-835f-1640f9c4a4b2"
      ]
    },
    "3946da4e-c994-4e00-8605-2222424ca5ed": {
      "id": "3946da4e-c994-4e00-8605-2222424ca5ed",
      "name": "FindBestPowerUp",
      "title": "Find best PowerUp",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": -1116
      }
    },
    "76b42b9d-6db0-4d81-a0fb-cc6d19b67270": {
      "id": "76b42b9d-6db0-4d81-a0fb-cc6d19b67270",
      "name": "MoveTo",
      "title": "Move to Next Location",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -324
      }
    },
    "3d779786-8677-4ad6-966d-4b4c3bf1d909": {
      "id": "3d779786-8677-4ad6-966d-4b4c3bf1d909",
      "name": "Sequence",
      "title": "Destroy wall",
      "description": "",
      "properties": {},
      "display": {
        "x": -216,
        "y": 1308
      },
      "children": [
        "d6893494-78e2-49fb-9d7e-e6e0f38b987a",
        "5f88bd24-94ef-4bc1-8fbf-6abbbc5c43bd",
        "20d5adda-f0d1-40e7-8557-cd81aab54afd",
        "434b05ea-9250-4447-b815-310ad0772275",
        "715f75ac-aeee-41c9-8bb0-e8b25af6f64f",
        "d288a11a-e771-447d-93d9-e70f9d7b8aed"
      ]
    },
    "d6893494-78e2-49fb-9d7e-e6e0f38b987a": {
      "id": "d6893494-78e2-49fb-9d7e-e6e0f38b987a",
      "name": "FindBestDestructibleWall",
      "title": "Find Best Destructible Wall",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1080
      }
    },
    "4381c484-61cf-4f2f-b03d-990c1620e98a": {
      "id": "4381c484-61cf-4f2f-b03d-990c1620e98a",
      "name": "Sequence",
      "title": "Detonate",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": -1512
      },
      "children": [
        "1e6ee724-50e6-4ba6-b25d-7e8d9b622f95",
        "8cf861b6-9bca-4eda-832d-6013d8896d55",
        "dfa37b1c-c6be-4d51-aafb-92c797689d69",
        "c30dc6e3-fd50-402d-888f-04a3c728a8be"
      ]
    },
    "09884a72-f283-47ff-a5a8-df079253007a": {
      "id": "09884a72-f283-47ff-a5a8-df079253007a",
      "name": "IsOnOwnBomb",
      "title": "On own Bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -1380
      }
    },
    "c30dc6e3-fd50-402d-888f-04a3c728a8be": {
      "id": "c30dc6e3-fd50-402d-888f-04a3c728a8be",
      "name": "DetonateBomb",
      "title": "Detonate Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -1296
      }
    },
    "a5c5325e-7656-4aba-972c-5fc882f3bd88": {
      "id": "a5c5325e-7656-4aba-972c-5fc882f3bd88",
      "name": "Priority",
      "title": "Get Next Move",
      "description": "",
      "properties": {},
      "display": {
        "x": -420,
        "y": 12
      },
      "children": [
        "ea5dd2fc-29eb-4e5d-9124-f3f1e2cf0a6b",
        "1768a424-d1a0-4cc4-8648-54097314ae1e",
        "81f83b43-941f-43f9-8df2-417aaf79308b",
        "c1bb797c-a695-4120-977f-07c284501f6a",
        "3d779786-8677-4ad6-966d-4b4c3bf1d909",
        "8f932c10-d148-4f8e-8dfc-4429537874f8",
        "b6eca350-60f1-49a8-8bf5-127370c03633"
      ]
    },
    "92dba9b4-fe52-4aed-b46b-8fe7909a3610": {
      "id": "92dba9b4-fe52-4aed-b46b-8fe7909a3610",
      "name": "GetPathToGoal",
      "title": "Get path to PowerUp",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": -1032
      }
    },
    "939a2eb1-56de-48f3-82f7-cafb47348816": {
      "id": "939a2eb1-56de-48f3-82f7-cafb47348816",
      "name": "GetNextLocationFromPath",
      "title": "Get next location from path",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": -936
      }
    },
    "e099df7c-3712-4162-879a-bbadf85efd49": {
      "id": "e099df7c-3712-4162-879a-bbadf85efd49",
      "name": "IsNextLocationWalkable",
      "title": "Is next location walkable?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -504
      }
    },
    "d2f9ec15-1038-4b91-8182-bd9be17fa4c0": {
      "id": "d2f9ec15-1038-4b91-8182-bd9be17fa4c0",
      "name": "GetPathToGoal",
      "title": "Get path to safe location",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -1992
      }
    },
    "90a50844-c515-4264-84a4-d53d9d9c83ec": {
      "id": "90a50844-c515-4264-84a4-d53d9d9c83ec",
      "name": "GetNextLocationFromPath",
      "title": "Get next location from path",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -1908
      }
    },
    "5f88bd24-94ef-4bc1-8fbf-6abbbc5c43bd": {
      "id": "5f88bd24-94ef-4bc1-8fbf-6abbbc5c43bd",
      "name": "GetPathToGoal",
      "title": "Get path to destructible wall",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1176
      }
    },
    "20d5adda-f0d1-40e7-8557-cd81aab54afd": {
      "id": "20d5adda-f0d1-40e7-8557-cd81aab54afd",
      "name": "GetNextLocationFromPath",
      "title": "Get next location from path",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1260
      }
    },
    "d288a11a-e771-447d-93d9-e70f9d7b8aed": {
      "id": "d288a11a-e771-447d-93d9-e70f9d7b8aed",
      "name": "MoveTo",
      "title": "Move to Next Location",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1524
      }
    },
    "434b05ea-9250-4447-b815-310ad0772275": {
      "id": "434b05ea-9250-4447-b815-310ad0772275",
      "name": "IsNextLocationWalkable",
      "title": "Is next location walkable?",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1344
      }
    },
    "07542c43-3854-4392-892a-ea6a761b4d63": {
      "id": "07542c43-3854-4392-892a-ea6a761b4d63",
      "name": "Priority",
      "title": "In danger?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -1428
      },
      "children": [
        "b61dee6b-9118-44b2-a8cc-5a2303072f19",
        "09884a72-f283-47ff-a5a8-df079253007a"
      ]
    },
    "dfa37b1c-c6be-4d51-aafb-92c797689d69": {
      "id": "dfa37b1c-c6be-4d51-aafb-92c797689d69",
      "name": "Inverter",
      "title": "Is safe to detonate?",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -1428
      },
      "child": "07542c43-3854-4392-892a-ea6a761b4d63"
    },
    "8f932c10-d148-4f8e-8dfc-4429537874f8": {
      "id": "8f932c10-d148-4f8e-8dfc-4429537874f8",
      "name": "Sequence",
      "title": "Plant Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": -216,
        "y": 1704
      },
      "children": [
        "41488d32-306f-48b6-a07c-dcf0b0f9b4c1",
        "3877f1dc-0b16-41d3-8145-c9b91ab21e99",
        "d969bf7a-cbaa-43c3-815e-f8999d8c6404"
      ]
    },
    "d969bf7a-cbaa-43c3-815e-f8999d8c6404": {
      "id": "d969bf7a-cbaa-43c3-815e-f8999d8c6404",
      "name": "PlantBomb",
      "title": "Plant Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1788
      }
    },
    "41488d32-306f-48b6-a07c-dcf0b0f9b4c1": {
      "id": "41488d32-306f-48b6-a07c-dcf0b0f9b4c1",
      "name": "CanPlantBomb",
      "title": "Has bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1608
      }
    },
    "3877f1dc-0b16-41d3-8145-c9b91ab21e99": {
      "id": "3877f1dc-0b16-41d3-8145-c9b91ab21e99",
      "name": "CanEvadeOwnBomb",
      "title": "Can evade own bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1704
      }
    },
    "b2d67a02-bda8-4782-801f-7233f40f9002": {
      "id": "b2d67a02-bda8-4782-801f-7233f40f9002",
      "name": "CanPlantBomb",
      "title": "Has bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -1560
      }
    },
    "571755f6-9531-45f3-80cc-d1a611ed000b": {
      "id": "571755f6-9531-45f3-80cc-d1a611ed000b",
      "name": "Inverter",
      "title": "No Bombs",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -1560
      },
      "child": "b2d67a02-bda8-4782-801f-7233f40f9002"
    },
    "47a34de0-1652-4018-905a-b7f6555682fd": {
      "id": "47a34de0-1652-4018-905a-b7f6555682fd",
      "name": "EnemyInBlastRadius",
      "title": "Is Enemy in blast range?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -1644
      }
    },
    "8cf861b6-9bca-4eda-832d-6013d8896d55": {
      "id": "8cf861b6-9bca-4eda-832d-6013d8896d55",
      "name": "Priority",
      "title": "Enemy in range or no bombs",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -1596
      },
      "children": [
        "47a34de0-1652-4018-905a-b7f6555682fd",
        "571755f6-9531-45f3-80cc-d1a611ed000b"
      ]
    },
    "c1bb797c-a695-4120-977f-07c284501f6a": {
      "id": "c1bb797c-a695-4120-977f-07c284501f6a",
      "name": "Sequence",
      "title": "Target Enemy",
      "description": "",
      "properties": {},
      "display": {
        "x": -216,
        "y": 264
      },
      "children": [
        "0ca63dc7-c7f7-4f20-aef2-25b8a6378f5f",
        "a9e26265-3d27-42f3-a4a8-d1531820f1e7",
        "51383ade-f94c-4ec1-b743-6516caf47afb"
      ]
    },
    "0ca63dc7-c7f7-4f20-aef2-25b8a6378f5f": {
      "id": "0ca63dc7-c7f7-4f20-aef2-25b8a6378f5f",
      "name": "FindBestEnemy",
      "title": "Find best enemy",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 24
      }
    },
    "51383ade-f94c-4ec1-b743-6516caf47afb": {
      "id": "51383ade-f94c-4ec1-b743-6516caf47afb",
      "name": "Priority",
      "title": "Bomb or advance on enemy",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 648
      },
      "children": [
        "97271f9c-2ad1-4d4c-907f-9dd3924dc47e",
        "cf66bf3e-40be-4785-a686-c662368f7b5d"
      ]
    },
    "97271f9c-2ad1-4d4c-907f-9dd3924dc47e": {
      "id": "97271f9c-2ad1-4d4c-907f-9dd3924dc47e",
      "name": "Sequence",
      "title": "Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": 420
      },
      "children": [
        "8006a803-ac4e-4554-a5a7-acfea7d91f52",
        "b816e71f-c692-4866-8462-c9dc507446ff",
        "a39dce40-4f39-42c6-a070-deab2d76db9a",
        "03ffe3dc-3ac3-4225-a748-0c0e3b677223",
        "d52b2d46-f2c5-4e77-8d5e-eb9798077c95"
      ]
    },
    "cf66bf3e-40be-4785-a686-c662368f7b5d": {
      "id": "cf66bf3e-40be-4785-a686-c662368f7b5d",
      "name": "Sequence",
      "title": "Advance",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": 864
      },
      "children": [
        "1cced5b9-e298-4054-8275-e610ee3f5695",
        "3b282499-bf25-4f27-85fe-59f4b4382f17",
        "fd993135-2383-49ae-8073-351257041c70",
        "6142f4ee-3b30-43be-8894-7f1bec1dcc24"
      ]
    },
    "0431782a-1c9d-4957-9780-4065ad88c4c1": {
      "id": "0431782a-1c9d-4957-9780-4065ad88c4c1",
      "name": "IsInBlastRadius",
      "title": "In Blast Radius?",
      "description": "",
      "properties": {},
      "display": {
        "x": 828,
        "y": 384
      }
    },
    "81c9d854-cfae-4a25-b74b-d1db14c6ecad": {
      "id": "81c9d854-cfae-4a25-b74b-d1db14c6ecad",
      "name": "IsOnOwnBomb",
      "title": "On own Bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 828,
        "y": 468
      }
    },
    "c3c95c37-b499-4ada-8a56-9bda6e4a1468": {
      "id": "c3c95c37-b499-4ada-8a56-9bda6e4a1468",
      "name": "Priority",
      "title": "In danger?",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": 420
      },
      "children": [
        "0431782a-1c9d-4957-9780-4065ad88c4c1",
        "81c9d854-cfae-4a25-b74b-d1db14c6ecad"
      ]
    },
    "a39dce40-4f39-42c6-a070-deab2d76db9a": {
      "id": "a39dce40-4f39-42c6-a070-deab2d76db9a",
      "name": "Inverter",
      "title": "Is safe to plant?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 420
      },
      "child": "c3c95c37-b499-4ada-8a56-9bda6e4a1468"
    },
    "a9e26265-3d27-42f3-a4a8-d1531820f1e7": {
      "id": "a9e26265-3d27-42f3-a4a8-d1531820f1e7",
      "name": "GetPathToGoal",
      "title": "Get path to Enemy",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 120
      }
    },
    "6142f4ee-3b30-43be-8894-7f1bec1dcc24": {
      "id": "6142f4ee-3b30-43be-8894-7f1bec1dcc24",
      "name": "MoveTo",
      "title": "Move to Next Location",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 996
      }
    },
    "1cced5b9-e298-4054-8275-e610ee3f5695": {
      "id": "1cced5b9-e298-4054-8275-e610ee3f5695",
      "name": "GetNextLocationFromPath",
      "title": "Get next location from path",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 732
      }
    },
    "3b282499-bf25-4f27-85fe-59f4b4382f17": {
      "id": "3b282499-bf25-4f27-85fe-59f4b4382f17",
      "name": "IsNextLocationWalkable",
      "title": "Is next location walkable?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 816
      }
    },
    "8006a803-ac4e-4554-a5a7-acfea7d91f52": {
      "id": "8006a803-ac4e-4554-a5a7-acfea7d91f52",
      "name": "IsEnemyInRange",
      "title": "Is Enemy in range",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 204
      }
    },
    "d52b2d46-f2c5-4e77-8d5e-eb9798077c95": {
      "id": "d52b2d46-f2c5-4e77-8d5e-eb9798077c95",
      "name": "PlantBomb",
      "title": "Plant Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 648
      }
    },
    "dff19ee9-5703-40e6-819d-c8eba0d00054": {
      "id": "dff19ee9-5703-40e6-819d-c8eba0d00054",
      "name": "IsLocationSafe",
      "title": "Is location safe?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -408
      }
    },
    "715f75ac-aeee-41c9-8bb0-e8b25af6f64f": {
      "id": "715f75ac-aeee-41c9-8bb0-e8b25af6f64f",
      "name": "IsLocationSafe",
      "title": "Is location safe?",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": 1440
      }
    },
    "b6eca350-60f1-49a8-8bf5-127370c03633": {
      "id": "b6eca350-60f1-49a8-8bf5-127370c03633",
      "name": "DoNothing",
      "title": "Do Nothing",
      "description": "",
      "properties": {},
      "display": {
        "x": -216,
        "y": 1872
      }
    },
    "1e6ee724-50e6-4ba6-b25d-7e8d9b622f95": {
      "id": "1e6ee724-50e6-4ba6-b25d-7e8d9b622f95",
      "name": "HasBombToDetonate",
      "title": "Has bomb to detonate",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -1728
      }
    },
    "748bfdd4-c8c0-474f-942b-77f3ede1ac9a": {
      "id": "748bfdd4-c8c0-474f-942b-77f3ede1ac9a",
      "name": "Sequence",
      "title": "Advance on PowerUp",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -408
      },
      "children": [
        "e099df7c-3712-4162-879a-bbadf85efd49",
        "dff19ee9-5703-40e6-819d-c8eba0d00054",
        "76b42b9d-6db0-4d81-a0fb-cc6d19b67270"
      ]
    },
    "0c0bdfdf-f6b4-43c5-835f-1640f9c4a4b2": {
      "id": "0c0bdfdf-f6b4-43c5-835f-1640f9c4a4b2",
      "name": "Priority",
      "title": "Advance on powerUp or Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": -432
      },
      "children": [
        "ea344f70-7fac-43c8-8dbc-28470400e51f",
        "748bfdd4-c8c0-474f-942b-77f3ede1ac9a",
        "6e181d88-ac47-4ec5-a756-f0c827b17547"
      ]
    },
    "6e181d88-ac47-4ec5-a756-f0c827b17547": {
      "id": "6e181d88-ac47-4ec5-a756-f0c827b17547",
      "name": "Sequence",
      "title": "Bomb PowerUp",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -144
      },
      "children": [
        "ab5c7fb4-c739-4563-a1ef-b03121f9f1a6",
        "9c1224eb-a7bd-46b5-bfb8-92ba74cb5cc4",
        "7dddfa5d-15db-42fd-85c9-e71c2914a053"
      ]
    },
    "7dddfa5d-15db-42fd-85c9-e71c2914a053": {
      "id": "7dddfa5d-15db-42fd-85c9-e71c2914a053",
      "name": "PlantBomb",
      "title": "Plant Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -60
      }
    },
    "ab5c7fb4-c739-4563-a1ef-b03121f9f1a6": {
      "id": "ab5c7fb4-c739-4563-a1ef-b03121f9f1a6",
      "name": "CanPlantBomb",
      "title": "Has bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -240
      }
    },
    "9c1224eb-a7bd-46b5-bfb8-92ba74cb5cc4": {
      "id": "9c1224eb-a7bd-46b5-bfb8-92ba74cb5cc4",
      "name": "CanEvadeOwnBomb",
      "title": "Can evade own bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -144
      }
    },
    "bf359c17-19be-481e-9b65-7a5cfd0b408e": {
      "id": "bf359c17-19be-481e-9b65-7a5cfd0b408e",
      "name": "Sequence",
      "title": "Evade Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": -2112
      },
      "children": [
        "1819e552-f73a-4f0c-94bb-265664eb57bc",
        "7b80412d-049a-47ea-8cc3-b451d58109d0",
        "4a67b6e2-7768-47cb-8dc4-55dec887c311",
        "d2f9ec15-1038-4b91-8182-bd9be17fa4c0",
        "90a50844-c515-4264-84a4-d53d9d9c83ec",
        "44f9d2dd-7149-4e4d-8f7f-404bba18336b"
      ]
    },
    "826d7350-8e24-41cb-8a86-68bd27e67975": {
      "id": "826d7350-8e24-41cb-8a86-68bd27e67975",
      "name": "CanPlantBomb",
      "title": "Has bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -3840
      }
    },
    "d6081c69-fd4c-4cbd-8547-d5f7a317391f": {
      "id": "d6081c69-fd4c-4cbd-8547-d5f7a317391f",
      "name": "FindNextBombChainLocation",
      "title": "Find next bomb chain location",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -3048
      }
    },
    "c4c0d5bb-b1c8-4de5-b7f3-e7346844f76d": {
      "id": "c4c0d5bb-b1c8-4de5-b7f3-e7346844f76d",
      "name": "GetPathToGoal",
      "title": "Get path to bomb chain location",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -2964
      }
    },
    "96d9ede8-062c-4521-8dff-fbe3f0856369": {
      "id": "96d9ede8-062c-4521-8dff-fbe3f0856369",
      "name": "MoveTo",
      "title": "Move to bomb chain location",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -2700
      }
    },
    "89273071-44f2-4425-822e-b364125abf14": {
      "id": "89273071-44f2-4425-822e-b364125abf14",
      "name": "GetNextLocationFromPath",
      "title": "Get next location from path",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -2880
      }
    },
    "8b25f5ac-3356-4aef-941c-7fb4cf157520": {
      "id": "8b25f5ac-3356-4aef-941c-7fb4cf157520",
      "name": "Priority",
      "title": "Advance or chain bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -3264
      },
      "children": [
        "4b9935b8-1372-402f-8323-59f8a28e4f4e",
        "63f5c88c-3e9e-42a6-8d8c-76cf773fe6a6"
      ]
    },
    "63f5c88c-3e9e-42a6-8d8c-76cf773fe6a6": {
      "id": "63f5c88c-3e9e-42a6-8d8c-76cf773fe6a6",
      "name": "Sequence",
      "title": "Advance to bomb chain location",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -2940
      },
      "children": [
        "3e4618d0-06ca-40c7-893e-176e98641225",
        "d6081c69-fd4c-4cbd-8547-d5f7a317391f",
        "c4c0d5bb-b1c8-4de5-b7f3-e7346844f76d",
        "89273071-44f2-4425-822e-b364125abf14",
        "7b9a8548-64d8-4697-846b-9ce6f2e336fa",
        "96d9ede8-062c-4521-8dff-fbe3f0856369"
      ]
    },
    "4b9935b8-1372-402f-8323-59f8a28e4f4e": {
      "id": "4b9935b8-1372-402f-8323-59f8a28e4f4e",
      "name": "Sequence",
      "title": "Plant chained bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -3576
      },
      "children": [
        "b37a53d0-098b-48a1-b12b-b006b46c0a86",
        "0b93386d-16dd-4f54-8295-3985f8ca9d11",
        "d3e8595e-a51a-4dad-9171-34df7b2bb560"
      ]
    },
    "b37a53d0-098b-48a1-b12b-b006b46c0a86": {
      "id": "b37a53d0-098b-48a1-b12b-b006b46c0a86",
      "name": "IsOnBombChainLocation",
      "title": "Is on bomb chain location",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -3672
      }
    },
    "d3e8595e-a51a-4dad-9171-34df7b2bb560": {
      "id": "d3e8595e-a51a-4dad-9171-34df7b2bb560",
      "name": "PlantBomb",
      "title": "Plant Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -3492
      }
    },
    "0b93386d-16dd-4f54-8295-3985f8ca9d11": {
      "id": "0b93386d-16dd-4f54-8295-3985f8ca9d11",
      "name": "CanEvadeOwnBomb",
      "title": "Can evade own bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -3576
      }
    },
    "1819e552-f73a-4f0c-94bb-265664eb57bc": {
      "id": "1819e552-f73a-4f0c-94bb-265664eb57bc",
      "name": "Priority",
      "title": "In danger?",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -2568
      },
      "children": [
        "adbdd1e3-c626-4933-8738-c00ee6dc11ee",
        "1024e1c4-01a0-4b0e-8469-a8e641d851f4"
      ]
    },
    "e878562f-4e11-40d7-a501-60e39c425a38": {
      "id": "e878562f-4e11-40d7-a501-60e39c425a38",
      "name": "Sequence",
      "title": "Chain bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": -3624
      },
      "children": [
        "826d7350-8e24-41cb-8a86-68bd27e67975",
        "2f3535fe-72ef-49a9-848b-38ceae4306b3",
        "8b25f5ac-3356-4aef-941c-7fb4cf157520"
      ]
    },
    "ea5dd2fc-29eb-4e5d-9124-f3f1e2cf0a6b": {
      "id": "ea5dd2fc-29eb-4e5d-9124-f3f1e2cf0a6b",
      "name": "Priority",
      "title": "Evade or chain Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": -216,
        "y": -2868
      },
      "children": [
        "e878562f-4e11-40d7-a501-60e39c425a38",
        "bf359c17-19be-481e-9b65-7a5cfd0b408e"
      ]
    },
    "ea344f70-7fac-43c8-8dbc-28470400e51f": {
      "id": "ea344f70-7fac-43c8-8dbc-28470400e51f",
      "name": "Sequence",
      "title": "Bomb obstacle in path",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -720
      },
      "children": [
        "b288d45b-bb8b-4ca5-8006-e5b14cd5fa3b",
        "18307cfd-8fb7-4d72-8405-9da6fc06642f",
        "62790573-729c-418a-8c5f-2790d7f8d303",
        "998257b3-5ea6-4c59-ab9c-ebb341bc5221"
      ]
    },
    "998257b3-5ea6-4c59-ab9c-ebb341bc5221": {
      "id": "998257b3-5ea6-4c59-ab9c-ebb341bc5221",
      "name": "PlantBomb",
      "title": "Plant Bomb",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -588
      }
    },
    "b288d45b-bb8b-4ca5-8006-e5b14cd5fa3b": {
      "id": "b288d45b-bb8b-4ca5-8006-e5b14cd5fa3b",
      "name": "CanPlantBomb",
      "title": "Has bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -852
      }
    },
    "62790573-729c-418a-8c5f-2790d7f8d303": {
      "id": "62790573-729c-418a-8c5f-2790d7f8d303",
      "name": "CanEvadeOwnBomb",
      "title": "Can evade own bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -672
      }
    },
    "18307cfd-8fb7-4d72-8405-9da6fc06642f": {
      "id": "18307cfd-8fb7-4d72-8405-9da6fc06642f",
      "name": "IsObstacleInBombRange",
      "title": "Is obstacle in bomb range",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -768
      }
    },
    "b816e71f-c692-4866-8462-c9dc507446ff": {
      "id": "b816e71f-c692-4866-8462-c9dc507446ff",
      "name": "CanPlantBomb",
      "title": "Has bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 288
      }
    },
    "03ffe3dc-3ac3-4225-a748-0c0e3b677223": {
      "id": "03ffe3dc-3ac3-4225-a748-0c0e3b677223",
      "name": "CanEvadeOwnBomb",
      "title": "Can evade own bomb?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 552
      }
    },
    "1768a424-d1a0-4cc4-8648-54097314ae1e": {
      "id": "1768a424-d1a0-4cc4-8648-54097314ae1e",
      "name": "Priority",
      "title": "Detonate or wait for own explosion",
      "description": "",
      "properties": {},
      "display": {
        "x": -216,
        "y": -1356
      },
      "children": [
        "4381c484-61cf-4f2f-b03d-990c1620e98a",
        "f058829c-e812-4d20-8fb2-be8d8e8f2847"
      ]
    },
    "f058829c-e812-4d20-8fb2-be8d8e8f2847": {
      "id": "f058829c-e812-4d20-8fb2-be8d8e8f2847",
      "name": "WaitForOwnAdjacentExplosion",
      "title": "Wait for own adjacent explosion",
      "description": "",
      "properties": {},
      "display": {
        "x": 0,
        "y": -1200
      }
    },
    "7b80412d-049a-47ea-8cc3-b451d58109d0": {
      "id": "7b80412d-049a-47ea-8cc3-b451d58109d0",
      "name": "Priority",
      "title": "Get goal",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -2304
      },
      "children": [
        "403f6f34-47de-4a16-9dd4-5b1d5ea2ae1b",
        "ac421577-d1b0-47cc-8e37-8115698be4ce",
        "280ad2f1-b772-46a3-826c-3e9364bc54d9",
        "894eb6c4-db7c-49a1-8a25-d603faa7d564"
      ]
    },
    "403f6f34-47de-4a16-9dd4-5b1d5ea2ae1b": {
      "id": "403f6f34-47de-4a16-9dd4-5b1d5ea2ae1b",
      "name": "FindBestPowerUp",
      "title": "Find best PowerUp",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -2436
      }
    },
    "ac421577-d1b0-47cc-8e37-8115698be4ce": {
      "id": "ac421577-d1b0-47cc-8e37-8115698be4ce",
      "name": "FindBestEnemy",
      "title": "Find best enemy",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -2352
      }
    },
    "280ad2f1-b772-46a3-826c-3e9364bc54d9": {
      "id": "280ad2f1-b772-46a3-826c-3e9364bc54d9",
      "name": "FindBestDestructibleWall",
      "title": "Find Best Destructible Wall",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -2256
      }
    },
    "894eb6c4-db7c-49a1-8a25-d603faa7d564": {
      "id": "894eb6c4-db7c-49a1-8a25-d603faa7d564",
      "name": "Succeeder",
      "title": "No Goal found",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": -2172
      }
    },
    "fd993135-2383-49ae-8073-351257041c70": {
      "id": "fd993135-2383-49ae-8073-351257041c70",
      "name": "IsLocationSafe",
      "title": "Is location safe?",
      "description": "",
      "properties": {},
      "display": {
        "x": 408,
        "y": 912
      }
    },
    "3e4618d0-06ca-40c7-893e-176e98641225": {
      "id": "3e4618d0-06ca-40c7-893e-176e98641225",
      "name": "Priority",
      "title": "Get goal",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -3276
      },
      "children": [
        "fb236844-c374-4811-9d5f-75b585ef78dc",
        "3b8372ec-0d25-42e8-8390-eab77cf73a8d",
        "5b900c41-b763-43c0-9425-71e549bf1e94",
        "332c10b3-aaa3-4306-8274-e8368534b456"
      ]
    },
    "fb236844-c374-4811-9d5f-75b585ef78dc": {
      "id": "fb236844-c374-4811-9d5f-75b585ef78dc",
      "name": "FindBestPowerUp",
      "title": "Find best PowerUp",
      "description": "",
      "properties": {},
      "display": {
        "x": 828,
        "y": -3408
      }
    },
    "3b8372ec-0d25-42e8-8390-eab77cf73a8d": {
      "id": "3b8372ec-0d25-42e8-8390-eab77cf73a8d",
      "name": "FindBestEnemy",
      "title": "Find best enemy",
      "description": "",
      "properties": {},
      "display": {
        "x": 828,
        "y": -3312
      }
    },
    "5b900c41-b763-43c0-9425-71e549bf1e94": {
      "id": "5b900c41-b763-43c0-9425-71e549bf1e94",
      "name": "FindBestDestructibleWall",
      "title": "Find Best Destructible Wall",
      "description": "",
      "properties": {},
      "display": {
        "x": 828,
        "y": -3228
      }
    },
    "332c10b3-aaa3-4306-8274-e8368534b456": {
      "id": "332c10b3-aaa3-4306-8274-e8368534b456",
      "name": "Succeeder",
      "title": "No Goal found",
      "description": "",
      "properties": {},
      "display": {
        "x": 828,
        "y": -3144
      }
    },
    "2f3535fe-72ef-49a9-848b-38ceae4306b3": {
      "id": "2f3535fe-72ef-49a9-848b-38ceae4306b3",
      "name": "IsInBlastRadius",
      "title": "In Blast Radius?",
      "description": "",
      "properties": {},
      "display": {
        "x": 204,
        "y": -3756
      }
    },
    "7b9a8548-64d8-4697-846b-9ce6f2e336fa": {
      "id": "7b9a8548-64d8-4697-846b-9ce6f2e336fa",
      "name": "IsLocationSafe",
      "title": "Is location safe?",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -2784
      }
    },
    "b61dee6b-9118-44b2-a8cc-5a2303072f19": {
      "id": "b61dee6b-9118-44b2-a8cc-5a2303072f19",
      "name": "IsInOwnBlastRadius",
      "title": "Is in own blast radius",
      "description": "",
      "properties": {},
      "display": {
        "x": 624,
        "y": -1464
      }
    }
  },
  "display": {
    "camera_x": 685.0250000372471,
    "camera_y": 2328.899999972906,
    "camera_z": 0.75,
    "x": -624,
    "y": 12
  },
  "custom_nodes": [
    {
      "name": "IsInBlastRadius",
      "category": "condition",
      "title": "In Blast Radius?",
      "description": null,
      "properties": {}
    },
    {
      "name": "FindBestPowerUp",
      "category": "action",
      "title": "Find Best Powerup",
      "description": null,
      "properties": {}
    },
    {
      "name": "MoveTo",
      "category": "action",
      "title": "Move to",
      "description": null,
      "properties": {}
    },
    {
      "name": "FindSafeLocation",
      "category": "action",
      "title": "Find Safe Location",
      "description": null,
      "properties": {}
    },
    {
      "name": "FindBestDestructibleWall",
      "category": "action",
      "title": "Find Best Destructible Wall",
      "description": null,
      "properties": {}
    },
    {
      "name": "PlantBomb",
      "category": "action",
      "title": "Plant Bomb",
      "description": null,
      "properties": {}
    },
    {
      "name": "IsOnOwnBomb",
      "category": "condition",
      "title": "On own Bomb?",
      "description": null,
      "properties": {}
    },
    {
      "name": "DetonateBomb",
      "category": "action",
      "title": "Detonate Bomb",
      "description": null,
      "properties": {}
    },
    {
      "name": "GetPathToGoal",
      "category": "action",
      "title": "Get path to goal",
      "description": null,
      "properties": {}
    },
    {
      "name": "GetNextLocationFromPath",
      "category": "action",
      "title": "Get next location from path",
      "description": null,
      "properties": {}
    },
    {
      "name": "IsNextLocationWalkable",
      "category": "condition",
      "title": "Is next location walkable?",
      "description": null,
      "properties": {}
    },
    {
      "name": "CanPlantBomb",
      "category": "condition",
      "title": "Has bomb?",
      "description": null,
      "properties": {}
    },
    {
      "name": "CanEvadeOwnBomb",
      "category": "condition",
      "title": "Can evade own bomb?",
      "description": null,
      "properties": {}
    },
    {
      "name": "EnemyInBlastRadius",
      "category": "condition",
      "title": "Is Enemy in blast range?",
      "description": null,
      "properties": {}
    },
    {
      "name": "FindBestEnemy",
      "category": "action",
      "title": "Find best enemy",
      "description": null,
      "properties": {}
    },
    {
      "name": "IsEnemyInRange",
      "category": "condition",
      "title": "Is Enemy in range",
      "description": null,
      "properties": {}
    },
    {
      "name": "IsLocationSafe",
      "category": "condition",
      "title": "Is location safe?",
      "description": null,
      "properties": {}
    },
    {
      "name": "DoNothing",
      "category": "action",
      "title": "Do Nothing",
      "description": null,
      "properties": {}
    },
    {
      "name": "HasBombToDetonate",
      "category": "condition",
      "title": "Has bomb to detonate",
      "description": null,
      "properties": {}
    },
    {
      "name": "FindNextBombChainLocation",
      "category": "action",
      "title": "Find next bomb chain location",
      "description": null,
      "properties": {}
    },
    {
      "name": "IsOnBombChainLocation",
      "category": "condition",
      "title": "Is on bomb chain location",
      "description": null,
      "properties": {}
    },
    {
      "name": "IsInOwnBlastRadius",
      "category": "condition",
      "title": "Is in own blast radius",
      "description": null,
      "properties": {}
    },
    {
      "name": "IsObstacleInBombRange",
      "category": "condition",
      "title": "Is obstacle in bomb range",
      "description": null,
      "properties": {}
    },
    {
      "name": "WaitForOwnAdjacentExplosion",
      "category": "action",
      "title": "Wait for own adjacent explosion",
      "description": null,
      "properties": {}
    }
  ]
}