"use strict";
const bt = require('../../../BT');
const Entity = require('../../../Entity');
class GetNextLocationFromPath extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let goalPath = tick.blackboard.get('goalPath');
        let nextLocation = new Entity.Location(goalPath[1]);
        if (nextLocation) {
            tick.blackboard.set('nextLocation', nextLocation);
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.GetNextLocationFromPath = GetNextLocationFromPath;
//# sourceMappingURL=GetNextLocationFromPath.js.map