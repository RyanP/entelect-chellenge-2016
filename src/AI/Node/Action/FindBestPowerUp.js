"use strict";
const bt = require('../../../BT');
const Entity = require('../../../Entity');
const GameGraph_1 = require('../../../GameGraph');
const _ = require('lodash');
class FindBestPowerUp extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let powerUpLocation = this.findBestPowerUp(g);
        if (powerUpLocation) {
            tick.blackboard.set('goalLocation', powerUpLocation);
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
    findBestPowerUp(graph) {
        let hero = graph.hero;
        let heroNode = graph.nodes('Player')
            .query()
            .filter({ entity__Key__is: hero.Key })
            .first();
        let enemies = graph.otherPlayers;
        let powerUpPaths = graph.closest(heroNode, {
            compare: powerUpNode => {
                if (powerUpNode.entity === 'PowerUp') {
                    // make sure we're the closest player to powerup otherwise we
                    // probably won't get there first.
                    let powerUpId = powerUpNode.get('entity').id;
                    let heroDistance = graph.distanceTo(powerUpId, GameGraph_1.Direction.OUT);
                    let hasCloserEnemies = false;
                    if (!(powerUpNode.get('entity') instanceof Entity.SuperPowerUp)) {
                        // always target super powerup regardless of enemy locations
                        hasCloserEnemies = this.powerUpHasCloserEnemies(graph, heroDistance, powerUpNode);
                    }
                    return !hasCloserEnemies;
                }
                return false;
            },
            direction: GameGraph_1.Direction.OUT
        });
        // If there is a super powerup then filter the targets based on pythagorian
        // distance so super power is targeted even if it's weighted distance
        // is greater     
        let superPowerUp = _(powerUpPaths)
            .map(e => e.end().get('entity'))
            .find(e => e instanceof Entity.SuperPowerUp);
        if (superPowerUp) {
            // calulate pythagorian distance but half it to bias superPowerUp
            superPowerUp.Location.dist = Math.sqrt(Math.pow(hero.Location.X - superPowerUp.Location.X, 2) + Math.pow(hero.Location.Y - superPowerUp.Location.Y, 2)) * 0.5;
            powerUpPaths = _(powerUpPaths)
                .filter(p => {
                // compare pythagoras dist of each powerup to determin if super powerUp
                // is closer
                let powerUp = p.end().get('entity').Location;
                if (powerUp == superPowerUp.Location)
                    return true;
                let distance = Math.sqrt(Math.pow(hero.Location.X - powerUp.X, 2) + Math.pow(hero.Location.Y - powerUp.Y, 2));
                if (superPowerUp.Location.dist >= distance)
                    return true;
                return false;
            })
                .value();
        }
        let powerUp;
        if (powerUpPaths.length) {
            powerUp = powerUpPaths[0].end().get('entity').Location;
        }
        return powerUp && graph.isInBlastRadius(powerUp) ? undefined : powerUp;
    }
    powerUpHasCloserEnemies(graph, heroDistance, powerUpNode) {
        let enemies = graph.closest(powerUpNode, {
            compare: enemyNode => {
                return enemyNode.entity === 'Player' && enemyNode.get('key') !== graph.hero.Key;
            },
            maxDepth: Math.round(heroDistance),
            direction: GameGraph_1.Direction.IN
        });
        return enemies.length > 0;
    }
}
exports.FindBestPowerUp = FindBestPowerUp;
//# sourceMappingURL=FindBestPowerUp.js.map