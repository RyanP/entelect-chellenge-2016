"use strict";
const bt = require('../../../BT');
const GameGraph_1 = require('../../../GameGraph');
class GetPathToGoal extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let goalPath = g.pathTo(tick.blackboard.get('goalLocation'), GameGraph_1.Direction.OUT);
        if (goalPath && goalPath.length >= 2) {
            tick.blackboard.set('goalPath', goalPath);
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.GetPathToGoal = GetPathToGoal;
//# sourceMappingURL=GetPathToGoal.js.map