import * as bt from '../../../BT'
import { GameGraph, Direction } from '../../../GameGraph' 

export class GetPathToGoal extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let goalPath = g.pathTo(tick.blackboard.get('goalLocation'), Direction.OUT)

    if (goalPath && goalPath.length >= 2) {
      tick.blackboard.set('goalPath', goalPath)
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE
  }
}