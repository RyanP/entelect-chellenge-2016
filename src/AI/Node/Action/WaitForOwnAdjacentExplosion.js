"use strict";
const bt = require('../../../BT');
const Enum_1 = require('../../Enum');
const _ = require('lodash');
class WaitForOwnAdjacentExplosion extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = g.hero;
        let neighbors = h.Location.neighbours;
        let adjacentBombBlastTiles = _(neighbors)
            .filter(location => g.isInBlastRadius(location))
            .map(location => {
            let bombs = g.getBombsFromBlastLocation(location);
            return _.find(bombs, bomb => bomb.Owner === h.Key);
        })
            .filter(bomb => bomb && bomb.Owner === h.Key)
            .value();
        if (adjacentBombBlastTiles.length) {
            tick.blackboard.set('move', Enum_1.MOVE.nothing);
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.WaitForOwnAdjacentExplosion = WaitForOwnAdjacentExplosion;
//# sourceMappingURL=WaitForOwnAdjacentExplosion.js.map