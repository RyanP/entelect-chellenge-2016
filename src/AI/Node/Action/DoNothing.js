"use strict";
const bt = require('../../../BT');
const Enum_1 = require('../../Enum');
class DoNothing extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        tick.blackboard.set('move', Enum_1.MOVE.nothing);
        return bt.State.SUCCESS;
    }
}
exports.DoNothing = DoNothing;
//# sourceMappingURL=DoNothing.js.map