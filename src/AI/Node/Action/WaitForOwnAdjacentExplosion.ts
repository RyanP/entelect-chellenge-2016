import * as bt from '../../../BT'
import { MOVE } from '../../Enum'
import * as Entity from '../../../Entity'
import { GameGraph } from '../../../GameGraph' 
import * as UnitGraph from "ug" 
import * as _ from 'lodash'

export class WaitForOwnAdjacentExplosion extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = g.hero

    let neighbors = h.Location.neighbours

    let adjacentBombBlastTiles = _(neighbors)
      .filter(location => g.isInBlastRadius(location))
      .map(location => {
        let bombs = g.getBombsFromBlastLocation(location)
        return _.find(bombs, bomb => bomb.Owner === h.Key)
      })
      .filter(bomb => bomb && bomb.Owner === h.Key)
      .value()

    if (adjacentBombBlastTiles.length) {
      tick.blackboard.set('move', MOVE.nothing)
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE  
  }

}