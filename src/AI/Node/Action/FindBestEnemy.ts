import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph, Direction } from '../../../GameGraph' 
import * as _ from 'lodash'

export class FindBestEnemy extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')

    let enemyLocation = this.findBestEnemy(g)

    if (enemyLocation) {
      tick.blackboard.set('goalLocation', enemyLocation)
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE
  }

  public findBestEnemy(graph: GameGraph): Entity.Location {
    let hero: Entity.Player = graph.hero
    let enemies = graph.otherPlayers
    let range = hero.BombRadius
    let areDestructibleWalls = graph.nodes('DestructibleWall').query().units().length > 0
    let heroBombCount = hero.BombBag + _.filter(graph.bombs, b => b.Owner == hero.Key).length

    let enemy: Entity.Location = _(enemies)
      .filter(e => {
        let isEnemyScoreHigher = e.Points >= hero.Points
        let enemyHasMoreBombs = e.BombBag > heroBombCount
        let enemeyHasHigherRadius = e.BombRadius > hero.BombRadius

        if (!enemeyHasHigherRadius && !enemyHasMoreBombs) return true

        if (isEnemyScoreHigher && areDestructibleWalls) return false

        if (heroBombCount < 2 && areDestructibleWalls) return false

        return true
      })
      .map((e: Entity.Player) => {
        let enemyLocation: Entity.Location = e.Location
        enemyLocation.dist = graph.distanceFromToRaw(hero.Location, enemyLocation)
        return enemyLocation
      })
      .minBy((o: Entity.Location) => o.dist)
    
    // if ((enemies && enemies.length < 2 && graph.numberOfPlayers > 2)) {
    //   // ignore range and target only enemy
    //   return enemies[0].Location
    // }

    if (enemy && enemy.dist >= 100) range = range + 100
    
    return enemy ? enemy : undefined

  }

}