import * as bt from '../../../BT'
import * as Entity from '../../../Entity'

export class GetNextLocationFromPath extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let goalPath = tick.blackboard.get('goalPath')
    let nextLocation = new Entity.Location(goalPath[1])

    if (nextLocation) {
      tick.blackboard.set('nextLocation', nextLocation)
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE
  }

}