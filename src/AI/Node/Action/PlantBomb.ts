import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph, Node } from '../../../GameGraph'
import { MOVE } from '../../Enum'
import * as _ from 'lodash'

export class PlantBomb extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    tick.blackboard.set('move', MOVE.bomb)
    return bt.State.SUCCESS
  }
}