import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph, Direction } from '../../../GameGraph' 
import * as _ from 'lodash'

export class FindBestDestructibleWall extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target

    let wallLocation = this.findWall(g)

    if (wallLocation) {
      tick.blackboard.set('goalLocation', wallLocation)
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE
  }

  public findWall(graph: GameGraph): Entity.Location {
    let hero: Entity.Player = graph.hero
    let heroNode = graph.node(hero.id)
    
    let wallPaths = graph.closest(heroNode,
      {
        compare: node => {
          let isWall = node.entity === 'DestructibleWall'
          let isWallInBlast = graph.isInBlastRadius(node.get('id'))
          return isWall && !isWallInBlast
        },
        direction: Direction.IN,
        count: 1
      }
    )

    let debug = _(wallPaths) 
      .map(p => {
        return `${p.end().get('id')} - ${graph.distanceTo(p.end().get('id'), Direction.IN)}`
      })
      .value()

    wallPaths = _(wallPaths)
      .map(p => p.end().get('entity'))
      .value()

    return wallPaths.length > 0 ? wallPaths[0].Location : undefined
  }
}