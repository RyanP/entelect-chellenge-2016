"use strict";
const bt = require('../../../BT');
const Enum_1 = require('../../Enum');
class PlantBomb extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        tick.blackboard.set('move', Enum_1.MOVE.bomb);
        return bt.State.SUCCESS;
    }
}
exports.PlantBomb = PlantBomb;
//# sourceMappingURL=PlantBomb.js.map