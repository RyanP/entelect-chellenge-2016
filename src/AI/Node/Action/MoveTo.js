"use strict";
const bt = require('../../../BT');
const Enum_1 = require('../../Enum');
class MoveTo extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let hn = g.node(h.id);
        let from = h.Location;
        let to = tick.blackboard.get('nextLocation');
        let move = this.getMoveDirection(from, to);
        if (move) {
            tick.blackboard.set('move', move);
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
    getMoveDirection(from, to) {
        let move;
        let dx = to.X - from.X;
        let dy = to.Y - from.Y;
        if (dx && dx < 0)
            move = Enum_1.MOVE.left;
        if (dx && dx > 0)
            move = Enum_1.MOVE.right;
        if (dy && dy < 0)
            move = Enum_1.MOVE.up;
        if (dy && dy > 0)
            move = Enum_1.MOVE.down;
        return move;
    }
}
exports.MoveTo = MoveTo;
//# sourceMappingURL=MoveTo.js.map