import * as bt from '../../../BT'
import { MOVE } from '../../Enum'

export class DoNothing extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    tick.blackboard.set('move', MOVE.nothing)
    return bt.State.SUCCESS
  }
}