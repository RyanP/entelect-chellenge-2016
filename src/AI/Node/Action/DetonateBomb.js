"use strict";
const bt = require('../../../BT');
const Enum_1 = require('../../Enum');
class DetonateBomb extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        tick.blackboard.set('move', Enum_1.MOVE.trigger);
        return bt.State.SUCCESS;
    }
}
exports.DetonateBomb = DetonateBomb;
//# sourceMappingURL=DetonateBomb.js.map