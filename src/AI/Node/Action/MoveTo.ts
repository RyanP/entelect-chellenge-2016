import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph, Node } from '../../../GameGraph' 
import { MOVE } from '../../Enum'
import * as _ from 'lodash'

export class MoveTo extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target
    let hn: Node = g.node(h.id)

    let from = h.Location
    let to: Entity.Location = tick.blackboard.get('nextLocation') 
    let move = this.getMoveDirection(from, to)
    if (move) {
      tick.blackboard.set('move', move)
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE
  }

  public getMoveDirection(from: Entity.Location, to: Entity.Location): MOVE {
    let move: MOVE
    let dx = to.X - from.X
    let dy = to.Y - from.Y

    if (dx && dx < 0) move = MOVE.left
    if (dx && dx > 0) move = MOVE.right
    if (dy && dy < 0) move = MOVE.up
    if (dy && dy > 0) move = MOVE.down

    return move
  }

}