"use strict";
const bt = require('../../../BT');
const Entity = require('../../../Entity');
const GameGraph_1 = require('../../../GameGraph');
const _ = require('lodash');
class FindNextBombChainLocation extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let goalLocation = tick.blackboard.get('goalLocation');
        if (goalLocation) {
            let nextLocation = this.findNextLocation(g, goalLocation);
            if (nextLocation) {
                tick.blackboard.set('goalLocation', nextLocation);
                return bt.State.SUCCESS;
            }
        }
        return bt.State.FAILURE;
    }
    findNextLocation(graph, goalLocation) {
        let hero = graph.hero;
        let minTimer = graph.getChainedBombsMinTimer(hero.id) - 3;
        let bombs = graph.getBombsFromBlastLocation(hero.Location);
        let blast = _.reduce(bombs, (acc, v) => _.concat(acc, graph.bombBlastNodes[v.id]), []);
        let goalPath = graph.pathTo(goalLocation);
        let next;
        let candidates = _(blast)
            .map((location) => {
            let l = new Entity.Location(location);
            l.dist = graph.distanceFromTo(location, goalLocation);
            return l;
        })
            .filter(safeLocation => {
            let safeNode = graph.node(safeLocation.toString());
            // don't include locations if an enemy could block us.
            let closerEnemies = _(graph.otherPlayers)
                .filter(enemy => {
                //return false
                let locationIsCovered = false;
                let enemyTimer = Math.min((enemy.BombBag * 3) + 1, 10);
                let enemyRange = enemy.BombRadius;
                let blast = graph.getNodesInRadius(enemy.Location, enemyRange);
                let enemyBombs = _.filter(graph.bombs, b => b.Owner == enemy.Key);
                let enemyDistToSafeNode = graph.distanceFromTo(enemy.id, safeLocation);
                let heroDistToSafeNode = graph.distanceTo(safeLocation);
                if (_.includes(blast, safeLocation.toString())) {
                    locationIsCovered = enemy.BombBag - enemyBombs.length > 0;
                }
                return enemyDistToSafeNode <= heroDistToSafeNode || locationIsCovered;
            })
                .value();
            return closerEnemies.length ? false : true;
        })
            .filter((location) => {
            if (location.toString() == hero.id)
                return false;
            if (!graph.getEntityAt(location).isWalkable)
                return false;
            let distanceFromHero = graph.distanceTo(location);
            let isGoodLocation = (location.X % 2 == 0 && location.Y % 2 == 0);
            //let isReachable = location.dist < (minTimer - distanceFromHero)
            let isReachable = location.dist > distanceFromHero && distanceFromHero < minTimer;
            return isGoodLocation && isReachable;
        })
            .value();
        //.minBy('dist')
        if (candidates.length > 1) {
            next = _(candidates)
                .filter(l => _.includes(goalPath, l.toString()))
                .minBy('dist');
        }
        else {
            next = candidates[0];
        }
        if (next) {
            let closerEnemy = graph.closest(graph.node(next.toString()), {
                compare: node => {
                    if (node.entity === 'Player' && node.get('id') !== hero.id && node.get('id') !== goalLocation.toString()) {
                        let enemyDist = graph.distanceFromTo(node.get('id'), next);
                        let heroDist = graph.distanceTo(next);
                        if (enemyDist < heroDist) {
                            return true;
                        }
                        return false;
                    }
                },
                direction: GameGraph_1.Direction.OUT,
            });
            if (closerEnemy.length)
                next = undefined;
        }
        return next;
    }
}
exports.FindNextBombChainLocation = FindNextBombChainLocation;
//# sourceMappingURL=FindNextBombChainLocation.js.map