"use strict";
const bt = require('../../../BT');
const GameGraph_1 = require('../../../GameGraph');
const _ = require('lodash');
class FindBestDestructibleWall extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let wallLocation = this.findWall(g);
        if (wallLocation) {
            tick.blackboard.set('goalLocation', wallLocation);
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
    findWall(graph) {
        let hero = graph.hero;
        let heroNode = graph.node(hero.id);
        let wallPaths = graph.closest(heroNode, {
            compare: node => {
                let isWall = node.entity === 'DestructibleWall';
                let isWallInBlast = graph.isInBlastRadius(node.get('id'));
                return isWall && !isWallInBlast;
            },
            direction: GameGraph_1.Direction.IN,
            count: 1
        });
        let debug = _(wallPaths)
            .map(p => {
            return `${p.end().get('id')} - ${graph.distanceTo(p.end().get('id'), GameGraph_1.Direction.IN)}`;
        })
            .value();
        wallPaths = _(wallPaths)
            .map(p => p.end().get('entity'))
            .value();
        return wallPaths.length > 0 ? wallPaths[0].Location : undefined;
    }
}
exports.FindBestDestructibleWall = FindBestDestructibleWall;
//# sourceMappingURL=FindBestDestructibleWall.js.map