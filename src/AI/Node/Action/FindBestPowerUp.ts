import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph, Direction } from '../../../GameGraph' 
import * as UnitGraph from "ug"
import * as _ from 'lodash'

export class FindBestPowerUp extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target

    let powerUpLocation = this.findBestPowerUp(g)

    if (powerUpLocation) {
      tick.blackboard.set('goalLocation', powerUpLocation)
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE
  }

  public findBestPowerUp(graph: GameGraph): Entity.Location {
    let hero: Entity.Player = graph.hero

    let heroNode = graph.nodes('Player')
      .query()
      .filter({ entity__Key__is: hero.Key })
      .first()
    
    let enemies = graph.otherPlayers

    let powerUpPaths: UnitGraph.Path[] = graph.closest(heroNode as UnitGraph.Node,
      {
        compare: powerUpNode => {
          if (powerUpNode.entity === 'PowerUp') {
            // make sure we're the closest player to powerup otherwise we
            // probably won't get there first.
            let powerUpId = powerUpNode.get('entity').id
            let heroDistance = graph.distanceTo(powerUpId, Direction.OUT)
            let hasCloserEnemies = false
            
            if (!(powerUpNode.get('entity') instanceof Entity.SuperPowerUp)) {
              // always target super powerup regardless of enemy locations
              hasCloserEnemies = this.powerUpHasCloserEnemies(graph, heroDistance, powerUpNode)
            }
            
            return !hasCloserEnemies
          } 
            return false
        },
        direction: Direction.OUT
      }
    )

    // If there is a super powerup then filter the targets based on pythagorian
    // distance so super power is targeted even if it's weighted distance
    // is greater     
    let superPowerUp: Entity.SuperPowerUp = _(powerUpPaths)
      .map(e => e.end().get('entity'))
      .find(e => e instanceof Entity.SuperPowerUp)   
    
    if (superPowerUp) {
      // calulate pythagorian distance but half it to bias superPowerUp
      superPowerUp.Location.dist = Math.sqrt(Math.pow(hero.Location.X - superPowerUp.Location.X, 2) + Math.pow(hero.Location.Y - superPowerUp.Location.Y, 2)) * 0.5
      powerUpPaths = _(powerUpPaths)
        .filter(p => {
          // compare pythagoras dist of each powerup to determin if super powerUp
          // is closer
          let powerUp: Entity.Location = p.end().get('entity').Location
          if (powerUp == superPowerUp.Location) return true
          let distance = Math.sqrt(Math.pow(hero.Location.X - powerUp.X, 2) + Math.pow(hero.Location.Y - powerUp.Y, 2))
          if (superPowerUp.Location.dist >= distance) return true
          return false
        })
        .value()
    }
    
    let powerUp: Entity.Location
    if (powerUpPaths.length) {
      powerUp = powerUpPaths[0].end().get('entity').Location
    }
    
    return powerUp && graph.isInBlastRadius(powerUp) ? undefined : powerUp
  }

  private powerUpHasCloserEnemies(graph: GameGraph, heroDistance: number, powerUpNode: UnitGraph.Node) {    
    let enemies = graph.closest(powerUpNode,
      {
        compare: enemyNode => {
          return enemyNode.entity === 'Player' && enemyNode.get('key') !== graph.hero.Key
        },
        maxDepth: Math.round(heroDistance),
        direction: Direction.IN
      }
    )
    return enemies.length > 0
  }

}