import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph, Direction, Node } from '../../../GameGraph' 
import * as _ from 'lodash'

export class FindSafeLocation extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let goalLocation = tick.blackboard.get('goalLocation')

    let safeLocation = this.findSafeLocation(g, goalLocation)

    if (safeLocation) {
      tick.blackboard.set('goalLocation', safeLocation)
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE  
  }

  private findSafeLocation(graph: GameGraph, goal: Entity.Location): Entity.Location {
    let hero = graph.hero
    // let threateningBombs = graph.getThreateningBombs(hero)
    // let biggestThreat = _.maxBy(threateningBombs, b => b.BombTimer)

    let maxMoves = graph.getChainedBombsMinTimer(hero.id)

    let safeNodes = graph.closest(graph.node(hero.id),
      {
        compare: node => {
          if (node.entity === 'Empty' || node.entity === 'PowerUp') {
            let entity = node.get('entity')
            if (!graph.isInBlastRadius(entity.id)) {
              // does safe location have multiple escape routes?
              let neighbors = entity.Location.neighbours
              let emptyNeighbours = _(neighbors)
                .map(location => graph.node(location.toString()))
                .filter(neighbor => {
                  let isEmpty = neighbor && neighbor.entity === 'Empty'
                  let isHero = neighbor && neighbor.get('id') == graph.hero.id
                  let isInEnemyBlast = false
                  if (neighbor && graph.isInBlastRadius(neighbor.get('id'))) {
                    let bombs = graph.getBombsFromBlastLocation(neighbor.get('id'))
                    let bomb = _.find(bombs, bomb => bomb.Owner !== graph.hero.id)
                    isInEnemyBlast = bomb instanceof Entity.Bomb ? true : false
                  }
                  return (isEmpty || isHero) && isInEnemyBlast
                })
                .value()

              return emptyNeighbours.length > 0
            }
            return false
          } else {
            return false
          }
        },
        direction: Direction.OUT,
        maxDepth: maxMoves,
        //count: 1
      }
    )

    let safeEntities: Entity.IEntity[] = _(safeNodes) 
      .map(path => path.end().get('entity'))
      .value()
    
    let closerThanEnemy = _(safeEntities)
      .filter(safeNode => {
        // don't include nodes we can't reach anyway
        let heroDistToSafeNode = graph.distanceTo(safeNode.id)
        if (heroDistToSafeNode > maxMoves) return false

        // don't include safe locations if an enemy could block us.
        let closerEnemies = _(graph.otherPlayers)
          .filter(enemy => {
            let locationIsCovered = false
            let enemyTimer = Math.min((enemy.BombBag * 3) + 1, 10)
            let enemyRange = enemy.BombRadius
            let blast = graph.getNodesInRadius(enemy.Location, enemyRange)
            let enemyBombs = _.filter(graph.bombs, b => b.Owner == enemy.Key)

            let enemyDistToSafeNode = graph.distanceFromTo(enemy.id, safeNode.id)

            if (_.includes(blast, safeNode.Location.toString())) {
              locationIsCovered = maxMoves > 2 && enemy.BombBag - enemyBombs.length > 0
            }

            // exclude if safeNode could be bombed by an enemy
            let xdiff = Math.abs(enemy.Location.X - safeNode.Location.X)
            let ydiff = Math.abs(enemy.Location.Y - safeNode.Location.Y)

            if (xdiff == 1 || ydiff == 1) {
              let enemyPath = graph.pathTo(enemy.id)
              let safePath = graph.pathTo(safeNode.id)
              if (_.intersection(enemyPath, safePath)) {
                locationIsCovered = enemyDistToSafeNode - 1 <= enemy.BombRadius
              }
            }

            return enemyDistToSafeNode <= heroDistToSafeNode || locationIsCovered
          })
          .value()
        
        return closerEnemies.length ? false : true
      })
      .value()
    
    if (!closerThanEnemy.length) {
      // prefer locations further from enemies
      let enemies = graph.otherPlayers
      let furtherFromEnemy = _(safeEntities)
        .orderBy(safeNode => {
          safeNode.Location.dist = -10
          _.forEach(enemies, enemy => {
            let d = graph.distanceFromTo(safeNode.Location, enemy.Location)
            let dTo = graph.distanceTo(safeNode.Location)
            if (dTo > maxMoves) d = -10
            safeNode.Location.dist = d > safeNode.Location.dist ? d : safeNode.Location.dist
          })
          return safeNode.Location.dist
        }, ['desc'])
        .value()
      return furtherFromEnemy.length ? furtherFromEnemy[0].Location : undefined
    }

    let closerToGoal    
    if (goal) {
       closerToGoal = _(closerThanEnemy)
        .orderBy(safeNode => {
          let distToGoal = graph.distanceFromTo(safeNode.Location, goal, Direction.IN)
          let heroDistToSafeNode = graph.distanceTo(safeNode.Location)

          safeNode.Location.dist = heroDistToSafeNode <= maxMoves ? distToGoal : Infinity
          return safeNode.Location.dist
        })
        .filter(e => e.Location.dist !== Infinity)
        //.groupBy(safeNode => safeNode.Location.dist)
        .value()
    }
    
    let safe = _(closerToGoal || closerThanEnemy)
      .map(l => {
        return `${l.id} | ${l.Location.dist}`
      })
      .value()

    // avoid locations flanked by blast
    let notFlankedByBlast = _(closerToGoal || closerThanEnemy)
      .filter(entity => {
        let neighbors = entity.Location.neighbours
        let blastNeighbors = _(neighbors)
          .filter(l => {
            return graph.isInBlastRadius(l)
          })
          .value()
        return blastNeighbors.length < 2
      })
      .value()
    
    let safeNode = notFlankedByBlast.length ? notFlankedByBlast[0] : closerToGoal[0]

    return safeNode ? safeNode.Location : undefined
  }

}