"use strict";
const bt = require('../../../BT');
class IsInBlastRadius extends bt.BaseNode {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        if (g.isInBlastRadius(h)) {
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.IsInBlastRadius = IsInBlastRadius;
//# sourceMappingURL=IsInBlastRadius.js.map