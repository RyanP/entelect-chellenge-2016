"use strict";
const bt = require('../../../BT');
class IsOnOwnBomb extends bt.BaseNode {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        if (h.Bomb) {
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.IsOnOwnBomb = IsOnOwnBomb;
//# sourceMappingURL=IsOnOwnBomb.js.map