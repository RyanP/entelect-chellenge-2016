"use strict";
const bt = require('../../../BT');
const Entity = require('../../../Entity');
const GameGraph_1 = require('../../../GameGraph');
const _ = require('lodash');
class CanEvadeOwnBomb extends bt.Condition {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let goalLocation = tick.blackboard.get('goalLocation');
        let escapePath = this.getEscapePath(g, goalLocation);
        if (escapePath) {
            tick.blackboard.set('goalPath', escapePath);
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
    getEscapePath(g, goal) {
        let h = g.hero;
        let bomb = new Entity.Bomb({
            BombRadius: h.BombRadius,
            BombTimer: Math.min((h.BombBag * 3) + 1, 10),
            IsExploding: false,
            Owner: h,
            Location: h.Location
        });
        let minTimer = g.getChainedBombsMinTimer(h.id);
        minTimer = minTimer == Infinity ? bomb.BombTimer : minTimer - 1;
        if (g.isInBlastRadius(h.id)) {
            let otherBombs = _.filter(g.getBombsFromBlastLocation(h.id), b => b.Owner !== h.Key);
            _.forEach(otherBombs, b => {
                let enemy = _.find(g.otherPlayers, p => b.Owner === p.Key);
                let enemyOnBomb = enemy && b.id === enemy.id;
                if (enemyOnBomb) {
                    minTimer = Math.min(2, minTimer);
                }
                else if (enemy) {
                    let enemyDist = Math.max(g.distanceFromTo(enemy.id, b.id) - 3, 1);
                    minTimer = Math.min(enemyDist, minTimer);
                }
                else {
                    minTimer = Math.min(b.BombTimer, minTimer);
                }
            });
        }
        // return early
        if (minTimer <= 0)
            return undefined;
        let blast = g.getNodesInRadius(bomb.Location, bomb.BombRadius);
        blast.push(h.id);
        // let inBlastRadius = n => _.includes(blast, n.id)
        // let inExistingBlastRadius = n => g.isInBlastRadius(n.id)
        let blastNodes = [];
        _.forEach(g.bombBlastNodes, v => {
            return blastNodes = _.concat(v, blastNodes);
        });
        blastNodes = _.concat(blastNodes, blast);
        let safeNodes = g.closest(g.getAt(h.id), {
            compare: node => {
                if (node.entity === 'Empty' || node.entity === 'PowerUp') {
                    let entity = node.get('entity');
                    let safePath = g.pathTo(entity.Location);
                    let intersection = _.intersection(safePath, blastNodes);
                    if (intersection.length < safePath.length) {
                        return intersection.length <= minTimer;
                    }
                    return false;
                }
            },
            maxDepth: Math.max(minTimer, 1),
            direction: GameGraph_1.Direction.OUT,
            count: 5,
        });
        safeNodes = _(safeNodes)
            .map(path => path.end().get('entity'))
            .value();
        let closerThanEnemy = _(safeNodes)
            .filter((safeNode) => {
            // don't include safe locations if an enemy could block us.
            let closerEnemies = _(g.otherPlayers)
                .filter(enemy => {
                let locationIsCovered = false;
                let enemyTimer = Math.min((enemy.BombBag * 3) + 1, 10);
                let enemyRange = enemy.BombRadius;
                let blast = g.getNodesInRadius(enemy.Location, enemyRange);
                if (_.includes(blast, safeNode.Location.toString())) {
                    locationIsCovered = enemy.BombBag > 0;
                }
                let enemyPath = g.pathFromTo(enemy.id, safeNode.id);
                let heroPath = g.pathTo(safeNode.id);
                let intersection = _.intersection(enemyPath, heroPath);
                if (!_.includes(intersection, h.id)) {
                    let dist = g.distanceFromToRaw(enemy.id, safeNode.id) - 1;
                    return dist <= g.distanceTo(safeNode.id) - 1 || locationIsCovered;
                }
                else {
                    return locationIsCovered;
                }
            })
                .value();
            // don't inlcude safeNodes adjacent to a blast node if enemy is close
            let neighborCoveredByBlast = _(safeNode.Location.neighbours)
                .find(n => {
                return g.isInBlastRadius(n) && closerEnemies.length;
            });
            if (neighborCoveredByBlast)
                return false;
            return closerEnemies.length ? false : true;
        })
            .value();
        if (!closerThanEnemy.length) {
            return undefined;
        }
        let safeNode = _(closerThanEnemy)
            .minBy(n => {
            let id = n.id;
            if (goal) {
                return g.distanceFromTo(goal, id);
            }
            else {
                return g.distanceTo(id);
            }
        });
        return safeNode ? g.pathTo(safeNode.Location) : undefined;
    }
}
exports.CanEvadeOwnBomb = CanEvadeOwnBomb;
//# sourceMappingURL=CanEvadeOwnBomb.js.map