import * as bt from '../../../BT'
import * as UnitGraph from "ug"
import * as Entity from '../../../Entity'
import { GameGraph } from '../../../GameGraph' 
import * as _ from 'lodash'

export class HasBombToDetonate extends bt.Action {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target

    let bomb = g.getPlayerBombWithLowestTimer(h.Key)
    
    return bomb ? bt.State.SUCCESS : bt.State.FAILURE
  }
}