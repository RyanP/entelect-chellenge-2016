import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph } from '../../../GameGraph'
import * as _ from 'lodash'

export class CanPlantBomb extends bt.Condition {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target

    let bombs: Entity.Bomb[] = g.heroBombs
    
    if (bombs.length < h.BombBag) {
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE
  }
}