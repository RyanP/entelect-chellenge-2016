"use strict";
const bt = require('../../../BT');
const _ = require('lodash');
class IsInOwnBlastRadius extends bt.BaseNode {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let chain = g.getChainedBombs(h.id);
        let chainedBlast = _.reduce(chain, (acc, v) => {
            return _.concat(acc, g.bombBlastNodes[v]);
        }, []);
        let inChainedBlast = _.includes(chainedBlast, h.id);
        if (g.isInOwnBlastRadius(h) || inChainedBlast) {
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.IsInOwnBlastRadius = IsInOwnBlastRadius;
//# sourceMappingURL=IsInOwnBlastRadius.js.map