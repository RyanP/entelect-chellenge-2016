"use strict";
const bt = require('../../../BT');
class IsNextLocationWalkable extends bt.Condition {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let nextLocation = tick.blackboard.get('nextLocation');
        if (nextLocation) {
            let node = g.getEntityAt(nextLocation);
            if (node.isWalkable) {
                return bt.State.SUCCESS;
            }
        }
        tick.blackboard.set('goalPath', undefined);
        tick.blackboard.set('goalLocation', undefined);
        tick.blackboard.set('nextLocation', undefined);
        return bt.State.FAILURE;
    }
}
exports.IsNextLocationWalkable = IsNextLocationWalkable;
//# sourceMappingURL=IsNextLocationWalkable.js.map