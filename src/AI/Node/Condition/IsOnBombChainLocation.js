"use strict";
const bt = require('../../../BT');
const Entity = require('../../../Entity');
const _ = require('lodash');
class IsOnBombChainLocation extends bt.BaseNode {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let location = h.Location;
        let isEven = location.X % 2 == 0 && location.Y % 2 == 0;
        let notOnOwnBomb = h.Bomb ? false : true;
        let goodLocation = isEven && notOnOwnBomb;
        let isPowerUpAvailable = _.find(g.powerUps, p => p instanceof Entity.SuperPowerUp) || false;
        if (goodLocation && !isPowerUpAvailable) {
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.IsOnBombChainLocation = IsOnBombChainLocation;
//# sourceMappingURL=IsOnBombChainLocation.js.map