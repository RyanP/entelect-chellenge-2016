"use strict";
const bt = require('../../../BT');
const _ = require('lodash');
class IsEnemyInRange extends bt.Condition {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let range = h.BombRadius;
        // let enemies = g.closest(g.node(h.id),
        //   {
        //     compare: enemyNode => {
        //       return enemyNode.entity === 'Player' && enemyNode.get('entity').Key !== g.hero.Key
        //     },
        //     //maxDepth: range,
        //     direction: Direction.IN
        //   }
        // )
        let enemies = g.otherPlayers;
        let candidates = _(enemies)
            .filter(e => {
            // would planting a bomb here have a high probablility of a kill?
            let d = g.distanceFromToRaw(h.id, e.id);
            let xdiff = Math.abs(e.Location.X - h.Location.X);
            let ydiff = Math.abs(e.Location.Y - h.Location.Y);
            if (xdiff == 1 || ydiff == 1) {
                // always in range if right next door, regardless of range
                return true;
            }
            if ((xdiff < 2 || ydiff < 2) && d <= range) {
                // enemy offset by at least 1 in each axis and bomb location maximises 
                // radius in both axis.
                if (h.Location.X % 2 == 0 && h.Location.Y % 2 == 0) {
                    return true;
                }
            }
        })
            .value();
        if (candidates.length)
            return bt.State.SUCCESS;
        return bt.State.FAILURE;
    }
}
exports.IsEnemyInRange = IsEnemyInRange;
//# sourceMappingURL=IsEnemyInRange.js.map