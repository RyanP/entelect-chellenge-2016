"use strict";
const bt = require('../../../BT');
const Entity = require('../../../Entity');
const GameGraph_1 = require('../../../GameGraph');
const _ = require('lodash');
class IsObstacleInBombRange extends bt.Condition {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let range = h.BombRadius;
        let goalPath = tick.blackboard.get('goalPath');
        let obstacleIndex = 0;
        let obstacle = _(goalPath)
            .map(p => g.getEntityAt(p))
            .find((e, index) => {
            if (e.isDestructible && e !== h) {
                obstacleIndex = index;
                return true;
            }
        });
        if (obstacleIndex) {
            // are we moving into a corridor longer than our blast radius?
            let nextBombLocation = new Entity.Location(goalPath[obstacleIndex - 1]);
            let BombTimer = Math.min((h.BombBag * 3) + 1, 10);
            let blast = g.getNodesInRadius(nextBombLocation, range);
            blast.push(nextBombLocation.toString());
            let inBlastRadius = n => _.includes(blast, n.id);
            let inExistingBlastRadius = n => g.isInBlastRadius(n.id);
            // temporarily reset hero edge weights
            let heroNode = g.node(h.id);
            _(heroNode.edges)
                .forEach(e => {
                if (e.inputNode.entity === 'Empty' || e.outputNode === 'Empty') {
                    e.setDistance(1);
                }
            });
            let safeNodes = g.closest(g.getAt(nextBombLocation), {
                compare: node => {
                    if (node.entity === 'Empty' || node.entity === 'PowerUp' || node.get('id') === h.id) {
                        let entity = node.get('entity');
                        if (!inBlastRadius(entity) && !inExistingBlastRadius(entity)) {
                            return true;
                        }
                        return false;
                    }
                },
                maxDepth: Math.max(BombTimer, 1),
                direction: GameGraph_1.Direction.OUT,
                count: 5,
            });
            safeNodes = _(safeNodes)
                .map(path => path.end().get('id'))
                .value();
            if (!safeNodes.length) {
                // are we next to obstacles that could clear the escape path
                let wallNeighbors = _(h.Location.neighbours)
                    .filter(l => g.getEntityAt(l) && g.getEntityAt(l).isDestructibleWall)
                    .value();
                if (wallNeighbors.length)
                    return bt.State.SUCCESS;
            }
        }
        if (obstacle && g.isLocationInRadius(h.Location, obstacle.Location, range)) {
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.IsObstacleInBombRange = IsObstacleInBombRange;
//# sourceMappingURL=IsObstacleInBombRange.js.map