import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph } from '../../../GameGraph' 
import * as _ from 'lodash'

export class IsOnBombChainLocation extends bt.BaseNode {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target
    let location = h.Location

    let isEven = location.X % 2 == 0 && location.Y % 2 == 0
    let notOnOwnBomb = h.Bomb ? false : true

    let goodLocation = isEven && notOnOwnBomb

    let isPowerUpAvailable = _.find(g.powerUps, p => p instanceof Entity.SuperPowerUp) || false
    
    if (goodLocation && !isPowerUpAvailable) {
      return bt.State.SUCCESS
    }
    return bt.State.FAILURE
  }
}