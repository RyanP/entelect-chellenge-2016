import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph } from '../../../GameGraph'

export class IsNextLocationWalkable extends bt.Condition {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph');
    let nextLocation = tick.blackboard.get('nextLocation')
    if (nextLocation) {
      let node: Entity.IEntity = g.getEntityAt(nextLocation)
      if (node.isWalkable) {
        return bt.State.SUCCESS
      }
    }
    tick.blackboard.set('goalPath', undefined)
    tick.blackboard.set('goalLocation', undefined)
    tick.blackboard.set('nextLocation', undefined)
    return bt.State.FAILURE
  }

}