import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph, Node } from '../../../GameGraph' 
import * as _ from 'lodash'

export class EnemyInBlastRadius extends bt.Condition {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target

    let players: Entity.Player[] = g.otherPlayers
    let bombs: Entity.Bomb[] = g.heroBombs
    let bombToDetonate = _(bombs).minBy((b: Entity.Bomb) => b.BombTimer)

    let killCandidates = _(players)
      .filter((p: Entity.Player) => g.isInBlastRadius(p.id))
      .value()
    
    if (killCandidates) {
      return bt.State.SUCCESS
    }
    
    return bt.State.FAILURE
  }
}