import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph } from '../../../GameGraph' 
import * as _ from 'lodash'

export class IsInBlastRadius extends bt.BaseNode {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target as Entity.Player

    if (g.isInBlastRadius(h)) {
      return bt.State.SUCCESS
    }
    return bt.State.FAILURE
  }
}