"use strict";
const bt = require('../../../BT');
class HasBombToDetonate extends bt.Action {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let bomb = g.getPlayerBombWithLowestTimer(h.Key);
        return bomb ? bt.State.SUCCESS : bt.State.FAILURE;
    }
}
exports.HasBombToDetonate = HasBombToDetonate;
//# sourceMappingURL=HasBombToDetonate.js.map