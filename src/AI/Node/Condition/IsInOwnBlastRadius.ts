import * as bt from '../../../BT'
import * as Entity from '../../../Entity'
import { GameGraph } from '../../../GameGraph' 
import * as _ from 'lodash'

export class IsInOwnBlastRadius extends bt.BaseNode {
  constructor(properties, id?: string) {
    super(properties, id);
  }

  tick(tick: bt.Tick) {
    let g: GameGraph = tick.blackboard.get('graph')
    let h: Entity.Player = tick.target as Entity.Player

    let chain = g.getChainedBombs(h.id)

    let chainedBlast = _.reduce(chain, (acc, v) => {
      return _.concat(acc, g.bombBlastNodes[v])
    }, [])

    let inChainedBlast = _.includes(chainedBlast, h.id)

    if (g.isInOwnBlastRadius(h) || inChainedBlast) { 
      return bt.State.SUCCESS
    }
    return bt.State.FAILURE
  }
}