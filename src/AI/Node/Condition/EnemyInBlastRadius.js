"use strict";
const bt = require('../../../BT');
const _ = require('lodash');
class EnemyInBlastRadius extends bt.Condition {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let players = g.otherPlayers;
        let bombs = g.heroBombs;
        let bombToDetonate = _(bombs).minBy((b) => b.BombTimer);
        let killCandidates = _(players)
            .filter((p) => g.isInBlastRadius(p.id))
            .value();
        if (killCandidates) {
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.EnemyInBlastRadius = EnemyInBlastRadius;
//# sourceMappingURL=EnemyInBlastRadius.js.map