"use strict";
const bt = require('../../../BT');
class CanPlantBomb extends bt.Condition {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let h = tick.target;
        let bombs = g.heroBombs;
        if (bombs.length < h.BombBag) {
            return bt.State.SUCCESS;
        }
        return bt.State.FAILURE;
    }
}
exports.CanPlantBomb = CanPlantBomb;
//# sourceMappingURL=CanPlantBomb.js.map