"use strict";
const bt = require('../../../BT');
const _ = require('lodash');
class IsLocationSafe extends bt.Condition {
    constructor(properties, id) {
        super(properties, id);
    }
    tick(tick) {
        let g = tick.blackboard.get('graph');
        let goalPath = tick.blackboard.get('goalPath');
        let nextLocation = tick.blackboard.get('nextLocation');
        let pathToGoalIsCorridor = this.pathIsCorridor(goalPath, g);
        let isEnemyClose = this.isEnemyClose(g);
        if (nextLocation) {
            let node = g.node(nextLocation.toString());
            let inBlastRadius = g.isInBlastRadius(nextLocation.toString());
            // does next location have multiple escape routes?
            let neighbors = nextLocation.neighbours;
            let emptyNeighbours = _(neighbors)
                .map(location => g.node(location.toString()))
                .filter(node => {
                let isEmpty = node && node.entity === 'Empty';
                let isHero = node && node.get('id') == g.hero.id;
                return isEmpty || isHero;
            })
                .value();
            // make sure the escape paths aren't covered by an enemy
            let safeNeighbours = _(emptyNeighbours)
                .filter(node => {
                let entity = node.get('entity');
                let safeX = entity.Location.X;
                let safeY = entity.Location.Y;
                let threateningEnemy = _.find(g.otherPlayers, enemy => {
                    let enemyX = enemy.Location.X;
                    let enemyY = enemy.Location.Y;
                    if (enemyX === safeX || enemyY === safeY) {
                        let enemyTimer = Math.min((enemy.BombBag * 3) + 1, 10);
                        let enemyRange = enemy.BombRadius;
                        if (enemyRange >= g.distanceFromTo(entity.id, enemy.id)) {
                            return true;
                        }
                    }
                    return false;
                });
                return threateningEnemy ? false : true;
            })
                .value();
            if (!inBlastRadius && safeNeighbours.length && !(pathToGoalIsCorridor && isEnemyClose)) {
                return bt.State.SUCCESS;
            }
        }
        return bt.State.FAILURE;
    }
    pathIsCorridor(path, graph) {
        if (path.length <= 2)
            return false;
        let safeNodes = _(path)
            .filter((location, index) => {
            if (index === 0 || index === path.length - 1)
                return false;
            let node = graph.node(location);
            let neighbors = node.get('entity').Location.neighbours;
            let emptyNeighbors = _(neighbors)
                .map(location => graph.node(location.toString()))
                .filter(node => {
                if (!node)
                    return false;
                let isOnPath = _.includes(path, node.get('id'));
                let isEmpty = node.entity === 'Empty';
                let isHero = node.get('id') == graph.hero.id;
                return isEmpty && !isOnPath;
            })
                .value();
            if (emptyNeighbors.length)
                return true;
        })
            .value();
        return safeNodes.length === 0; //safeNodes.length !== path.length - 2
    }
    isEnemyClose(graph) {
        let enemies = graph.otherPlayers;
        let closeEnemies = _(enemies)
            .filter(enemy => graph.distanceTo(enemy.Location) < 3)
            .value();
        if (closeEnemies.length)
            return true;
        return false;
    }
}
exports.IsLocationSafe = IsLocationSafe;
//# sourceMappingURL=IsLocationSafe.js.map