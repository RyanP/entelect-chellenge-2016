"use strict";
(function (MOVE) {
    MOVE[MOVE["nothing"] = 0] = "nothing";
    MOVE[MOVE["up"] = 1] = "up";
    MOVE[MOVE["left"] = 2] = "left";
    MOVE[MOVE["right"] = 3] = "right";
    MOVE[MOVE["down"] = 4] = "down";
    MOVE[MOVE["bomb"] = 5] = "bomb";
    MOVE[MOVE["trigger"] = 6] = "trigger";
})(exports.MOVE || (exports.MOVE = {}));
var MOVE = exports.MOVE;
//# sourceMappingURL=Enum.js.map