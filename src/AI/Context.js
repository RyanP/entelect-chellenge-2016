"use strict";
const BT_1 = require('../BT');
class Context extends BT_1.BlackBoard {
    constructor(_graph, _treeScope) {
        super();
        this._graph = _graph;
        this._treeScope = _treeScope;
        this.set('graph', _graph);
    }
    set(key, value, nodeScope) {
        var memory = this.getMemory(this._treeScope, nodeScope);
        memory[key] = value;
    }
    get(key, nodeScope) {
        var memory = this.getMemory(this._treeScope, nodeScope);
        return memory[key];
    }
}
exports.Context = Context;
//# sourceMappingURL=Context.js.map