export enum MOVE {
    nothing = 0,
    up = 1,
    left = 2,
    right = 3,
    down = 4,
    bomb = 5,
    trigger = 6,
}