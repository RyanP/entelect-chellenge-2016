"use strict";
const BT_1 = require("../BT");
const NodeTypes = require('./Node');
const _1 = require('./');
const treeData_1 = require('./treeData');
class AI {
    constructor(_graph) {
        this._graph = _graph;
        this.brain = new BT_1.BehaviorTree();
        this.brain.load(treeData_1.treeData, NodeTypes);
        this.context = new _1.Context(_graph, treeData_1.treeData.id);
    }
    getMove() {
        this.brain.tick(this._graph.hero, this.context);
        let path = this.context.get('goalPath');
        let move = this.context.get('move');
        this._graph.draw(path || []);
        return move;
    }
}
exports.AI = AI;
//# sourceMappingURL=AI.js.map