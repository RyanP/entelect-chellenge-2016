import { BlackBoard } from '../BT'
import { GameGraph } from '../GameGraph'

export class Context extends BlackBoard {

  constructor(private _graph: GameGraph, private _treeScope) {
    super()

    this.set('graph', _graph)
  }

  public set(key, value, nodeScope?) {
    var memory = this.getMemory(this._treeScope, nodeScope);

    memory[key] = value;
  }
  
  public get(key, nodeScope?) {
    var memory = this.getMemory(this._treeScope, nodeScope);

    return memory[key];
  }

}