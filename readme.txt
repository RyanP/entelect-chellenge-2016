## Build Instructions:

This is a NodeJs submission so it can be built by simply running 
`npm install` in the root directory.

**Note:** The codebase is written in TypeScript and transpiled to Javascript. I
am including both the .js and .ts files so transpiling is not required on the 
server.

## Project Structure
Project structure is similar to the sample Javascript bot. I decided to 
use TypeSript for my code base as I wanted to learn something new while doing
this. Source code is in the `src` folder and consists of the following ...
* `main.ts` - bot entry point.
* `Logger.ts` - Logging module.
* `Config.ts` - Config module containing some debug options and config.
* `Entity/` - Entity and Factory classes representing all game entities
* `BT/` - A partial TypeScript reimplementation of the Behavior3.js 
  behavior tree library.
* `AI/` - Bot AI containing Action and Condition nodes for the behabior tree. 
  Also in this folder, is the treeData.ts file which contains the behavior tree
  definition JSON.

## Basic Strategy

My strategy is primarily implemented as a behavior tree which uses data 
within the GameGraph class to make decisions. The high level strategy is a 
delicate balance between:
* evading, detonating and chaining bombs
* targeting & collecting powerups 
* Targeting enemies
* destroying walls if they're obstacles to a goal location or if there's nothing
  else to do
* sometimes though doing nothing is the best move

All pathfinding & targeting is handled by the GameGraph which implements a 
weighted directed graph represtening the map. The input and output edge weights 
are set to guide the bot toward goal locations, while avoiding dangerous nodes.







